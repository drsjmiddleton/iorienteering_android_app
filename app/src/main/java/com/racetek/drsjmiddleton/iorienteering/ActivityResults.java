package com.racetek.drsjmiddleton.iorienteering;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.autosqlite.drsjmiddleton.autosqlite.DatabaseLoader;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseParams;
import com.autosqlite.drsjmiddleton.autosqlite.DbLoaderCallback;

import java.util.ArrayList;

public class ActivityResults extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DbLoaderCallback {


    ListView resultsListView;
    TextView noresultsTextView;

    LocalBroadcastManager lbm;

    String username;

    ArrayList<CourseAttempt> courseAttemptList;

    ResultListAdapter resultListAdapter;

    DatabaseLoader courseAttemptDBLoader;

    SharedPreferences sharedPreferences;

    final Context thisActivityContext = this;


    //Receives the responses from the reseultsenderservice
    private final BroadcastReceiver HTTPResponseReceiver = new BroadcastReceiver() {


        @Override
        public void onReceive(Context context, Intent intent) {

            CourseAttempt thisCA = intent.getParcelableExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT);

            if (intent.getBooleanExtra(StaticProjectValues.INTENT_RESULTS_SEND_SUCCESS, false)) {

                courseAttemptList.get(courseAttemptList.indexOf(thisCA)).uploadStatus = CourseAttempt.UPLOADED;

            }

            View affectedView = resultListAdapter.getViewFromId((long) courseAttemptList.get(courseAttemptList.indexOf(thisCA)).pk.hashCode());

            if (affectedView == null) {

                return;
            }

            final Button uploadResult = (Button) affectedView.findViewById(R.id.results_list_upload_button);

            final ProgressBar uploadProgress = (ProgressBar) affectedView.findViewById(R.id.results_list_upload_progress);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

                uploadResult.setVisibility(View.VISIBLE);
                uploadResult.animate().setDuration(shortAnimTime).alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        uploadResult.setVisibility(View.VISIBLE);
                    }
                });

                uploadProgress.setVisibility(View.GONE);
                uploadProgress.animate().setDuration(shortAnimTime).alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        uploadProgress.setVisibility(View.GONE);
                    }
                });
            } else {

                uploadProgress.setVisibility(View.GONE);
                uploadResult.setVisibility(View.VISIBLE);
            }

            resultListAdapter.notifyDataSetChanged();


        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.results_base);
        Toolbar toolbar = (Toolbar) findViewById(R.id.results_toolbar);
        toolbar.setTitle("Results");
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.results_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.results_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        resultsListView = (ListView) findViewById(R.id.results_results_list);
        noresultsTextView = (TextView) findViewById(R.id.results_no_results_message);

        lbm = LocalBroadcastManager.getInstance(this);

        IntentFilter receiveHTTPMultiResponsefilter = new IntentFilter(StaticProjectValues.INTENT_RESPONSE_FROM_RESULTS_SEND);
        lbm.registerReceiver(HTTPResponseReceiver, receiveHTTPMultiResponsefilter);


        courseAttemptList = new ArrayList<>();
        resultListAdapter = new ResultListAdapter(courseAttemptList, this);

        resultsListView.setAdapter(resultListAdapter);
    }

    @Override
    public void onResume() {

        populateFromDB();
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.results_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_view_home) {

            Intent startHomeIntent = new Intent(this, ActivityUserHome.class);

            startActivity(startHomeIntent);
            // Handle the camera action
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.results_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class ResultListAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<CourseAttempt> courseAttemptList = new ArrayList<CourseAttempt>();
        private Context context;


        public ResultListAdapter(ArrayList<CourseAttempt> courseAttemptList, Context context) {
            this.courseAttemptList = courseAttemptList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return courseAttemptList.size();
        }

        @Override
        public Object getItem(int pos) {
            return courseAttemptList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return (long) courseAttemptList.get(pos).pk.hashCode();

        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        public View getViewFromId(long Id) {

            for (int i = 0; i < getCount(); i++) {

                if (getItemId(i) == Id) {

                    return getView(i, null, null);
                }

            }

            return null;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.item_results_list, null);
            }

            //Handle TextView and display string from your list
            TextView courseNameView = (TextView) view.findViewById(R.id.results_list_course_name);
            courseNameView.setText(courseAttemptList.get(position).course.courseName);
            TextView areaNameView = (TextView) view.findViewById(R.id.results_list_area_name);
            areaNameView.setText(courseAttemptList.get(position).course.areaName);
            TextView dateTimeView = (TextView) view.findViewById(R.id.results_list_date_time);
            dateTimeView.setText(courseAttemptList.get(position).attemptTime.toString("dd/MM/yyyy HH:mm:ss"));

            //Handle buttons and add onClickListeners
            Button viewResult = (Button) view.findViewById(R.id.results_list_enter_button);

            final Button uploadResult = (Button) view.findViewById(R.id.results_list_upload_button);

            final ProgressBar uploadProgress = (ProgressBar) view.findViewById(R.id.results_list_upload_progress);

            if (courseAttemptList.get(position).uploadStatus == CourseAttempt.UPLOADED) {

                uploadResult.setEnabled(false);
            }


            viewResult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent startSingleResultIntent = new Intent(context, ActivitySingleResult.class);

                    startSingleResultIntent.putExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT, courseAttemptList.get(position));

                    startActivity(startSingleResultIntent);

                }
            });

            uploadResult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent startResultUploadIntent = new Intent(context, ServiceResultSender.class);

                    startResultUploadIntent.putExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT, courseAttemptList.get(position));

                    startService(startResultUploadIntent);


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

                        uploadResult.setVisibility(View.GONE);
                        uploadResult.animate().setDuration(shortAnimTime).alpha(0).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                uploadResult.setVisibility(View.GONE);
                            }
                        });

                        uploadProgress.setVisibility(View.VISIBLE);
                        uploadProgress.animate().setDuration(shortAnimTime).alpha(1).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                uploadProgress.setVisibility(View.VISIBLE);
                            }
                        });
                    } else {
                        // The ViewPropertyAnimator APIs are not available, so simply show
                        // and hide the relevant UI components.
                        uploadProgress.setVisibility(View.VISIBLE);
                        uploadResult.setVisibility(View.GONE);
                    }


                }
            });


            return view;
        }
    }

    @Override
    public void onDBLoadFinished(int startId) {
        switch (startId) {

            case COURSE_ATTEMPT_DB_LOADER_START_ID:

                if (courseAttemptDBLoader.returnList == null || courseAttemptDBLoader.returnList.size() == 0) {

                    resultsListView.setVisibility(View.GONE);
                    noresultsTextView.setVisibility(View.VISIBLE);


                } else {

                    resultsListView.setVisibility(View.VISIBLE);
                    noresultsTextView.setVisibility(View.GONE);

                    courseAttemptList.clear();
                    for (Object courseAttempt : courseAttemptDBLoader.returnList) {

                        CourseAttempt mCA = (CourseAttempt) courseAttempt;

                        if (mCA.finished || mCA.retired) {

                            courseAttemptList.add((CourseAttempt) courseAttempt);
                        }

                    }


                    if (courseAttemptList.size() == 0) {

                        resultsListView.setVisibility(View.GONE);
                        noresultsTextView.setVisibility(View.VISIBLE);
                    } else {

                        resultListAdapter.notifyDataSetChanged();
                    }
                }
                courseAttemptDBLoader = null;

                break;

            default:
                break;
        }
    }

    private void populateFromDB() {

        if (courseAttemptDBLoader == null) {


            courseAttemptDBLoader = new DatabaseLoader();

            DatabaseParams courseAttemptDblp = new DatabaseParams(
                    COURSE_ATTEMPT_DB_LOADER_START_ID,
                    CourseAttempt.class,
                    StaticProjectValues.NAME_OF_COURSE_ATTEMPT_DATABASE,
                    this,
                    DatabaseParams.LOAD_ALL);

            Object[] argsArray = {courseAttemptDblp};

            courseAttemptDBLoader.setCallback(this).execute(argsArray);

        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.

    }

    private final int COURSE_ATTEMPT_DB_LOADER_START_ID = 3;
}
