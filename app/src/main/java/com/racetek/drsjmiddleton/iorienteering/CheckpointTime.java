package com.racetek.drsjmiddleton.iorienteering;

import android.os.Parcel;
import android.os.Parcelable;

import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteIgnoreFieldAnnotation;
import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLitePrimaryKeyAnnotation;
import com.google.common.collect.ComparisonChain;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;

/**
 * Created by drsjmiddleton on 01/09/2016.
 */
public class CheckpointTime implements Parcelable {

    @AutoSQLitePrimaryKeyAnnotation
    String pk;
    String courseAttemptPk;
    String checkpointID;
    int cpOrder;
    DateTime checkpointTime;
    int cptStatus;


    public CheckpointTime() {

        //empty constructor for AutoSQLlite

    }

    public CheckpointTime(String courseAttemptPk, String checkpointID, int cpOrder, DateTime checkpointTime, int cptStatus) {
        this.courseAttemptPk = courseAttemptPk;
        this.checkpointID = checkpointID;
        this.cpOrder = cpOrder;
        this.checkpointTime = checkpointTime;
        this.cptStatus = cptStatus;
    }

    public CheckpointTime(String pk, String courseAttemptPk, String checkpointID, int cpOrder, DateTime checkpointTime) {

        this.pk = pk;
        this.courseAttemptPk = courseAttemptPk;
        this.checkpointID = checkpointID;
        this.cpOrder = cpOrder;
        this.checkpointTime = checkpointTime;
        this.cptStatus = CORRECT_CPT;
    }

    public CheckpointTime(Parcel in) {

        this.pk = in.readString();
        this.courseAttemptPk = in.readString();
        this.checkpointID = in.readString();
        this.cpOrder = in.readInt();
        this.checkpointTime = new DateTime(in.readLong());
        this.cptStatus = in.readInt();


    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeString(pk);
        out.writeString(courseAttemptPk);
        out.writeString(checkpointID);
        out.writeInt(cpOrder);
        out.writeLong(checkpointTime.getMillis());
        out.writeInt(cptStatus);

    }

    @AutoSQLiteIgnoreFieldAnnotation
    public static final Parcelable.Creator<CheckpointTime> CREATOR
            = new Parcelable.Creator<CheckpointTime>() {
        public CheckpointTime createFromParcel(Parcel in) {
            return new CheckpointTime(in);
        }

        public CheckpointTime[] newArray(int size) {
            return new CheckpointTime[size];
        }
    };

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getCourseAttemptPk() {
        return courseAttemptPk;
    }

    public void setCourseAttemptPk(String courseAttemptPk) {
        this.courseAttemptPk = courseAttemptPk;
    }

    public String getCheckpointID() {
        return checkpointID;
    }

    public void setCheckpointID(String checkpointID) {
        this.checkpointID = checkpointID;
    }

    public int getCpOrder() {
        return cpOrder;
    }

    public void setCpOrder(int cpOrder) {
        this.cpOrder = cpOrder;
    }

    public DateTime getCheckpointTime() {
        return checkpointTime;
    }

    public void setCheckpointTime(DateTime checkpointTime) {
        this.checkpointTime = checkpointTime;
    }

    public int getCptStatus() {
        return cptStatus;
    }

    public void setCptStatus(int cptStatus) {
        this.cptStatus = cptStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CheckpointTime that = (CheckpointTime) o;

        return getPk().equals(that.getPk());

    }

    @Override
    public int hashCode() {
        return getPk().hashCode();
    }

    public static class cptOrderComparator implements Comparator<CheckpointTime> {
        public int compare(CheckpointTime cpt1, CheckpointTime cpt2) {
            return ComparisonChain.start()
                    .compare(cpt1.getCpOrder(), cpt2.getCpOrder())
                    .result();
        }
    }

    public static class singletoMapCptOrderComparator implements Comparator<Map<Integer, CheckpointTime>> {
        public int compare(Map<Integer, CheckpointTime> map1, Map<Integer, CheckpointTime> map2) {

            CheckpointTime cpt1 = (CheckpointTime) map1.values().toArray()[0];
            CheckpointTime cpt2 = (CheckpointTime) map2.values().toArray()[0];


            return ComparisonChain.start()
                    .compare(cpt1.getCpOrder(), cpt2.getCpOrder())
                    .result();
        }
    }

    public static String timeLongToChronoString(long milli) {

        int h = (int) (milli / 3600000);
        int m = (int) (milli - h * 3600000) / 60000;
        int s = (int) (milli - h * 3600000 - m * 60000) / 1000;
        String hh = h < 10 ? "0" + h : h + "";
        String mm = m < 10 ? "0" + m : m + "";
        String ss = s < 10 ? "0" + s : s + "";

        return hh + ":" + mm + ":" + ss;


    }

    public static boolean doesListContainSameCheckpoint(ArrayList<CheckpointTime> cptList, CheckpointTime cpt) {

        if (cptList == null || cpt == null) {

            return false;
        }


        for (CheckpointTime mCpt : cptList) {

            if (mCpt.checkpointID.equals(cpt.checkpointID)) {


                return true;
            }

        }

        return false;

    }

    @AutoSQLiteIgnoreFieldAnnotation
    public final static int CORRECT_CPT = 0;
    @AutoSQLiteIgnoreFieldAnnotation
    public final static int MISSING_CPT = 1;
    @AutoSQLiteIgnoreFieldAnnotation
    public final static int WRONG_ORDER_CPT = 2;


}
