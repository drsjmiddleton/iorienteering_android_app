package com.racetek.drsjmiddleton.iorienteering;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by drsjmiddleton on 04/01/2017.
 */

@Root(name = "SplitTime")
public class SplitTime {

    @Element(name = "ControlCode")
    String controlCode;
    @Element(name = "Time")
    double time;

    public SplitTime(String controlCode, double time) {
        this.controlCode = controlCode;
        this.time = time;
    }

    public String getControlCode() {
        return controlCode;
    }

    public double getTime() {
        return time;
    }
}
