package com.racetek.drsjmiddleton.iorienteering;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ActivityLoginScreen extends AppCompatActivity {

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    int timeout;


    private UserLoginTask mAuthTask = null;

    private EditText mUserNameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    private String authToken = null;
    private String LOG_TAG = "LOG_TAG";
    private boolean loginSuccess = false;

    private String userName;
    private String password;

    private OkHttpClient HTTPClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen_base);

        Context appContext = getApplicationContext();

        String appPackageName = appContext.getPackageName();

        String appPackageCodePath = appContext.getPackageCodePath();

        sharedpreferences = getSharedPreferences(StaticProjectValues.SHARED_PREFS, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        HTTPClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();

        //if user is already logged continue to race list


        if ((sharedpreferences.getBoolean(StaticProjectValues.SHARED_PREFS_IS_LOGGED_IN, false))) {


            Intent startUserHome = new Intent(this, ActivityUserHome.class);
            startActivity(startUserHome);
            return;

        }

        editor.putString(StaticProjectValues.SHARED_PREFS_API_TOKEN, "");
        editor.putString(StaticProjectValues.SHARED_PREFS_USERNAME, "");
        editor.apply();

        timeout = 10;

        // Set up the login form.
        mUserNameView = (EditText) findViewById(R.id.login_user_name);


        mPasswordView = (EditText) findViewById(R.id.login_password);


        Button signInButton = (Button) findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

    }

    @Override
    public void onBackPressed() {
        return;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void testLogin() {

        //testing

        Intent startUserHome = new Intent(this, ActivityUserHome.class);
        startActivity(startUserHome);
        return;

    }


    public void attemptLogin() {

        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUserNameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        userName = mUserNameView.getText().toString();
        password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a password
        if (TextUtils.isEmpty(password)) {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        }

        // Check for a userName
        if (TextUtils.isEmpty(userName)) {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);


            mAuthTask = new UserLoginTask(userName, password);
            mAuthTask.execute((Void) null);


        }
    }

    public void postLogin() {

        editor.putString(StaticProjectValues.SHARED_PREFS_USERNAME, userName);
        editor.putString(StaticProjectValues.SHARED_PREFS_API_TOKEN, authToken);
        editor.putBoolean(StaticProjectValues.SHARED_PREFS_IS_LOGGED_IN, true);
        editor.apply();

        Intent startRaceList = new Intent(this, ActivityUserHome.class);
        startActivity(startRaceList);

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;
        private final String mPassword;
        private int sendAttemptCounter = 0;

        UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;

        }

        @Override
        protected Boolean doInBackground(Void... params) {


            authToken = loginToWeb(mUsername, mPassword);

            if (authToken != null) {


                loginSuccess = true;

            }

            return loginSuccess;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;

            showProgress(false);

            if (success) {

                postLogin();


            } else {
                mPasswordView.setError("Login Failed - please check connection and details and try again");
                mPasswordView.requestFocus();

            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

        private String loginToWeb(String username, String password) {

            RequestBody formBody = new FormBody.Builder()
                    .add("username", username)
                    .add("password", password)
                    .build();

            Request request = new Request.Builder()
                    .url(StaticProjectValues.API_GET_TOKEN)
                    .post(formBody)
                    .build();

            for (int i = 0; i < 5; i++) {


                try {
                    Response response = HTTPClient.newCall(request).execute();

                    String retSrc = response.body().string();

                    try {
                        JSONObject jsonToken = new JSONObject(retSrc);

                        return jsonToken.getString("token");

                    } catch (JSONException e) {

                        return null;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            return null;
        }

    }
}
