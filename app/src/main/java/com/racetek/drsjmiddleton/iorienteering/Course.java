package com.racetek.drsjmiddleton.iorienteering;

import android.os.Parcel;
import android.os.Parcelable;

import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteIgnoreFieldAnnotation;
import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLitePrimaryKeyAnnotation;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by drsjmiddleton on 01/09/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Course implements Parcelable, Comparable {


    @AutoSQLitePrimaryKeyAnnotation
    @JsonProperty("pk")
    String pk;
    @JsonProperty("area")
    String areaPk;
    @JsonProperty("course_name")
    String courseName;
    @JsonProperty("area_name")
    String areaName;
    @JsonProperty("course_codes")
    String courseCodes;
    @JsonProperty("course_points")
    String coursePoints;

    @JsonProperty("course_description")
    String courseDescription;

    @JsonProperty("distance")
    double length;
    @JsonProperty("penalty")
    double penalty;

    @JsonProperty("allow_multiple_runs")
    boolean allowMultipleRuns;
    @JsonProperty("is_score")
    boolean isScore;

    @JsonProperty("time_limit")
    int timeLimit;

    DateTime dateCreated;

    @AutoSQLiteIgnoreFieldAnnotation
    HashMap<String, Integer> checkpointPointsMap;


    public Course() {

        //empty constructor for AutoSQLlite

    }

    public Course(Parcel in) {

        this.pk = in.readString();
        this.areaPk = in.readString();
        this.courseName = in.readString();
        this.areaName = in.readString();
        this.courseCodes = in.readString();
        this.coursePoints = in.readString();
        this.courseDescription = in.readString();


        this.length = in.readDouble();
        this.penalty = in.readDouble();

        this.allowMultipleRuns = in.readByte() != 0;
        this.isScore = in.readByte() != 0;

        this.timeLimit = in.readInt();

        this.dateCreated = new DateTime(in.readLong());

    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeString(pk);
        out.writeString(areaPk);
        out.writeString(courseName);
        out.writeString(areaName);
        out.writeString(courseCodes);
        out.writeString(coursePoints);
        out.writeString(courseDescription);

        out.writeDouble(length);
        out.writeDouble(penalty);

        out.writeByte((byte) (allowMultipleRuns ? 1 : 0));
        out.writeByte((byte) (isScore ? 1 : 0));

        out.writeInt(timeLimit);

        out.writeLong(dateCreated.getMillis());

    }

    @AutoSQLiteIgnoreFieldAnnotation
    public static final Parcelable.Creator<Course> CREATOR
            = new Parcelable.Creator<Course>() {
        public Course createFromParcel(Parcel in) {
            return new Course(in);
        }

        public Course[] newArray(int size) {
            return new Course[size];
        }
    };

    public boolean getIsScore() {
        return isScore;
    }

    public void setIsScore(boolean score) {
        isScore = score;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getAreaPk() {
        return areaPk;
    }

    public void setAreaPk(String areaPk) {
        this.areaPk = areaPk;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCourseCodes() {
        return courseCodes;
    }

    public void setCourseCodes(String courseCodes) {
        this.courseCodes = courseCodes;
    }

    public String getCoursePoints() {
        return coursePoints;
    }

    public void setCoursePoints(String coursePoints) {
        this.coursePoints = coursePoints;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getPenalty() {
        return penalty;
    }

    public void setPenalty(double penalty) {
        this.penalty = penalty;
    }

    public boolean getAllowMultipleRuns() {
        return allowMultipleRuns;
    }

    public void setAllowMultipleRuns(boolean allowMultipleRuns) {
        this.allowMultipleRuns = allowMultipleRuns;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public DateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(DateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        return getPk().equals(course.getPk());

    }


    @Override
    public int hashCode() {
        return getPk().hashCode();
    }

    public int compareTo(Object aThat) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        //this optimization is usually worthwhile, and can
        //always be added
        if (this == aThat) return EQUAL;

        final Course that = (Course) aThat;

        //primitive numbers follow this form
        if (this.dateCreated.getMillis() < that.dateCreated.getMillis()) return AFTER;
        if (this.dateCreated.getMillis() > that.dateCreated.getMillis()) return BEFORE;

        return EQUAL;
    }


    //public methods

    public int getSecondsToWholeScorePoint() {

        if (!isScore) {
            return 1;
        } else {

            return (int) Math.ceil(1 / penalty);
        }
    }

    public void populatePointsMap() {

        checkpointPointsMap = new HashMap<>();

        ArrayList<String> checkpointIdsList = Lists.newArrayList(
                Splitter.fixedLength(3)
                        .split(courseCodes));

        ArrayList<String> checkpointPointsList = Lists.newArrayList(
                Splitter.on(',')
                        .trimResults()
                        .omitEmptyStrings()
                        .split(coursePoints));


        for (int i = 0; i < checkpointIdsList.size(); i++) {

            try {
                checkpointPointsMap.put(checkpointIdsList.get(i), Integer.valueOf(checkpointPointsList.get(i)));
            } catch (NumberFormatException e) {
                checkpointPointsMap.put(checkpointIdsList.get(i), 0);
            }

        }

    }

    public boolean updateCourse(Course cloudCourse) {

        boolean changeFound = false;

        if (cloudCourse == null || getClass() != cloudCourse.getClass()) return changeFound;

        if (Double.compare(cloudCourse.getLength(), getLength()) != 0) {
            length = cloudCourse.getLength();
            changeFound = true;
        }
        if (Double.compare(cloudCourse.getPenalty(), getPenalty()) != 0) {
            penalty = cloudCourse.getPenalty();
            changeFound = true;
        }
        if (getAllowMultipleRuns() != cloudCourse.getAllowMultipleRuns()) {

            allowMultipleRuns = cloudCourse.getAllowMultipleRuns();
            changeFound = true;
        }
        if (isScore != cloudCourse.isScore) {

            isScore = cloudCourse.isScore;
            changeFound = true;
        }
        if (getTimeLimit() != cloudCourse.getTimeLimit()) {

            timeLimit = cloudCourse.getTimeLimit();
            changeFound = true;
        }
        if (!getCourseName().equals(cloudCourse.getCourseName())) {

            courseName = cloudCourse.getCourseName();
            changeFound = true;
        }
        if (!getCourseCodes().equals(cloudCourse.getCourseCodes())) {

            courseCodes = cloudCourse.getCourseCodes();
            changeFound = true;
        }
        if (!getCoursePoints().equals(cloudCourse.getCoursePoints())) {

            coursePoints = cloudCourse.getCoursePoints();
            changeFound = true;
        }

        return changeFound;
    }



}
