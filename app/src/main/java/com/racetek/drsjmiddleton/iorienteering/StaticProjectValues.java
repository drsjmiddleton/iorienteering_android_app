package com.racetek.drsjmiddleton.iorienteering;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by drsjmiddleton on 01/09/2016.
 */

public abstract class StaticProjectValues {


    public final static String SHARED_PREFS = "**com.drsjmiddleton.iOrienteering.Shared.Prefs**";
    public final static String SHARED_PREFS_API_TOKEN = "**SHARED_PREFS_API_TOKEN**";
    public final static String SHARED_PREFS_USERNAME = "**SHARED_PREFS_USERNAME**";
    public final static String SHARED_PREFS_IS_LOGGED_IN = "**SHARED_PREFS_IS_LOGGED_IN**";
    public final static String SHARED_PREFS_PK_OF_CURRENT_ATTEMPT = "**SHARED_PREFS_PK_OF_CURRENT_ATTEMPT**";

    public final static String API_GET_TOKEN = "http://iorienteering.racetek.webfactional.com/website/api/competitor_get_token/";
    public final static String API_GET_AREA_LIST = "http://iorienteering.racetek.webfactional.com/website/api/competitor_get_area_list/";
    public final static String API_GET_AREA_COURSE_LIST = "http://iorienteering.racetek.webfactional.com/website/api/**AREA_PK**/get_courses/";
    public final static String API_POST_RESULT = "http://iorienteering.racetek.webfactional.com/website/api/competitor_load_result/";


    private static final Map<String, String> LEGACY_CODES;

    static {
        Map<String, String> tempMap = new HashMap<String, String>();

        tempMap.put("0a0467d9580ee856f5ef81441f978e1d", "101");
        tempMap.put("2eaebf19d71eaf49dcaebf736f86fe9c", "102");
        tempMap.put("0af76521961535237765253b0cf9a017", "103");
        tempMap.put("2fabbf85fd80ce89871538ea4ff30c87", "104");
        tempMap.put("2637336bf1243a8d2b6854fc67345a3c", "105");
        tempMap.put("0fe048f2f2346a5969a98201da770d01", "106");
        tempMap.put("28138f783dcc2fc30b8221a38becb1ff", "107");
        tempMap.put("2e4183b8e67e2b16893569c0a5123fee", "108");
        tempMap.put("077f46e72d02e11c0050fe6e31100761", "109");
        tempMap.put("1de26ffafa87a166e6a5e17e58d92082", "110");

        tempMap.put("236f8ec70120e6cd354453b4cc936fde", "111");
        tempMap.put("1d14355bc2876a0039bf510ed226bed5", "112");
        tempMap.put("165b990b5c06f1d87c145126208c346c", "113");
        tempMap.put("29bb5bed154fef949486ce1ad0927881", "114");
        tempMap.put("14afab0a3552d67bd9b0fec88f4431f1", "115");
        tempMap.put("0855259c3a3bd75b203fff58e62fba61", "116");
        tempMap.put("20d91919efd0e86005ee2bdd4dc182da", "117");
        tempMap.put("2594323ffcf40f85107649cec6bf9aba", "118");
        tempMap.put("0c8a9fbb14f48ab52b34c20fe3c0190a", "119");
        tempMap.put("089a1a8e96aee88dab52b2eea1d15c1a", "120");

        tempMap.put("047dbfc074086f805174330c3e21f6fe", "121");
        tempMap.put("179f35e7cad0f68a3fc76ee0c33aeade", "122");
        tempMap.put("10e9eca138754df48ccc8516e579a6d7", "123");
        tempMap.put("1d65d2908a037c6f9586dcc74a6ea64f", "124");
        tempMap.put("03df5993de644fed34377abf88bc3d8d", "125");
        tempMap.put("2f71503aaca923d8a8d263b5a4029195", "126");
        tempMap.put("0cdc97c8c24a19e24086263bbcce3f54", "127");
        tempMap.put("2e04da148ddad29fd332e631bcb02332", "128");
        tempMap.put("1d4a9eae97865d5e0f62b0a643b52062", "129");
        tempMap.put("0afd4e359a5346a352a30fb0d75c7828", "130");

        tempMap.put("2368012b080888f1bfc98d840ce4f7da", "131");
        tempMap.put("04937ee0a0a23149f90683ecaa19c136", "132");
        tempMap.put("1505989415f234a8d2f9fa56290e8b1c", "133");
        tempMap.put("1ad35ff6967ac13937da3f41a15e62a7", "134");
        tempMap.put("25603f5ebc16baca5fbb19aef48e58e7", "135");
        tempMap.put("228324cee47e5377121801e6cbabad0e", "136");
        tempMap.put("137edf53cff5eda5cc078ef3d51080b9", "137");
        tempMap.put("24100c93dc6bfe5574261757c1964888", "138");
        tempMap.put("0af8827f8bd4371f0e380c6de5d7ebef", "139");
        tempMap.put("161a50017585b7da553c036bf72b418a", "140");

        LEGACY_CODES = Collections.unmodifiableMap(tempMap);
    }

    public final static String SETUP_CODE = "&&&";
    public final static String URL_PREFIX = "www.iOrienteering.com/?";

    public final static String INTENT_THIS_COURSE_ATTEMPT = "**INTENT_THIS_COURSE_ATTEMPT**";
    public final static String INTENT_THIS_AREA = "**INTENT_THIS_AREA**";


    public final static String INTENT_RESPONSE_FROM_RESULTS_SEND = "**INTENT_RESPONSE_FROM_RESULTS_SEND**";
    public final static String INTENT_RESULTS_SEND_SUCCESS = "**INTENT_RESULTS_SEND_SUCCESS**";


    public final static String NAME_OF_COURSE_DATABASE = "DATABASE_IORIENTEERING_COURSES";
    public final static String NAME_OF_COURSE_ATTEMPT_DATABASE = "DATABASE_IORIENTEERING_COURSE_ATTEMPTS";

    public final static String[] DATABASE_NAMES_ARRAY = {NAME_OF_COURSE_ATTEMPT_DATABASE, NAME_OF_COURSE_DATABASE};
    public final static Class[] DATABASE_CLASSES_ARRAY = {CourseAttempt.class, Course.class};


    public static final int COURSE_DB_LOADER_START_ID = 1;
    public static final int COURSE_ATTEMPT_DB_LOADER_START_ID = 2;

    public final static String[] BUNDLE_CONFIRM_LOGOUT = {"Confirm Logout", "Are you SURE you want to logout? (You will need an internet connection to log back in)", "1"};



}
