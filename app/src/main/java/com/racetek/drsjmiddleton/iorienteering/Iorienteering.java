package com.racetek.drsjmiddleton.iorienteering;

import android.app.Application;
import android.content.Context;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by drsjmiddleton on 13/12/2016.
 */
public class Iorienteering extends Application {

    private static Context mContext;
    private static Iorienteering instance;


    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        mContext = getApplicationContext();
        instance = this;
    }
}
