package com.racetek.drsjmiddleton.iorienteering;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.support.v4.content.LocalBroadcastManager;

import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteDbFromObjectHelper;
import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteIncorrectObjectForDbException;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseInserter;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseParams;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;


public class ServiceResultSender extends IntentService {

    SharedPreferences sharedPreferences;
    String username;
    String token;
    ArrayList<String> sendAttemptedList;

    LocalBroadcastManager lbm;

    public ServiceResultSender() {
        super("ServiceResultSender");
    }

    @Override
    public void onCreate() {

        super.onCreate();
        sharedPreferences = getSharedPreferences(StaticProjectValues.SHARED_PREFS, Context.MODE_PRIVATE);
        username = sharedPreferences.getString(StaticProjectValues.SHARED_PREFS_USERNAME, null);
        token = sharedPreferences.getString(StaticProjectValues.SHARED_PREFS_API_TOKEN, null);
        sendAttemptedList = new ArrayList<>();
        lbm = LocalBroadcastManager.getInstance(this);

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        final CourseAttempt thisCA;
        final ValidatedCourseAttempt thisVca;
        final XMLCourseAttempt thisXMLVca;

        boolean sendSuccessful = false;

        if (token == null || username == null) {

            return;
        }


        if (intent != null) {

            thisCA = intent.getParcelableExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT);

            if (thisCA == null) {

                return;
            }

            if (sendAttemptedList.contains(thisCA.pk)) {
                return;
            } else {

                sendAttemptedList.add(thisCA.pk);
            }


            thisVca = thisCA.validateAttempt();

            thisXMLVca = new XMLCourseAttempt(thisVca);

            final String runDate = thisCA.attemptTime.toLocalDate().toString("yyyy-MM-dd");

            ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>() {{


            }};


            ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>() {{
                add(new NameValuePair("username", username));
                add(new NameValuePair("course", thisCA.course.pk));
                add(new NameValuePair("splits_xml", thisVca.generateXML()));
                add(new NameValuePair("area", thisCA.course.areaPk));
                add(new NameValuePair("status", String.valueOf(thisVca.status)));
                add(new NameValuePair("run_date", runDate));
                add(new NameValuePair("points", String.valueOf(thisVca.scorePoints - thisVca.scorePenalty)));
                add(new NameValuePair("run_time", String.valueOf(thisXMLVca.getTime())));
                add(new NameValuePair("dsq_reason", thisVca.dsqReason));

            }};


            ApiRetriever resultSender = new ApiRetriever(StaticProjectValues.API_POST_RESULT, headers, parameters, token, ApiRetriever.POST, 5);


            if (resultSender.responseCode == 202 || resultSender.responseCode == 409) {

                thisCA.uploadStatus = CourseAttempt.UPLOADED;

                sendSuccessful = true;


                AutoSQLiteDbFromObjectHelper dbHelper = new AutoSQLiteDbFromObjectHelper(getApplicationContext(), new ArrayList<Class>(Collections.singletonList(CourseAttempt.class)), StaticProjectValues.NAME_OF_COURSE_ATTEMPT_DATABASE);

                try {

                    dbHelper.insertObjectIntoDb(thisCA);

                } catch (AutoSQLiteIncorrectObjectForDbException e) {


                } catch (SQLException e) {

                }


            }


            Intent sendResultBackIntent = new Intent(StaticProjectValues.INTENT_RESPONSE_FROM_RESULTS_SEND);

            sendResultBackIntent.putExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT, thisCA);

            sendResultBackIntent.putExtra(StaticProjectValues.INTENT_RESULTS_SEND_SUCCESS, sendSuccessful);


            lbm.sendBroadcast(sendResultBackIntent);


        }
    }


}
