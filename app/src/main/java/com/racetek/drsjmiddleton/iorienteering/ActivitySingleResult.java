package com.racetek.drsjmiddleton.iorienteering;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ActivitySingleResult extends AppCompatActivity {

    private TextView courseResultTimeView;
    private TextView courseResultInfoView;
    private ListView splitsView;

    CourseAttempt thisCourseAttempt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_result_base);
        Toolbar toolbar = (Toolbar) findViewById(R.id.single_result_toolbar);
        toolbar.setTitle("Result");
        setSupportActionBar(toolbar);


        splitsView = (ListView) findViewById(R.id.single_result_splits_list);
        courseResultTimeView = (TextView) findViewById(R.id.single_result_time_points);
        courseResultInfoView = (TextView) findViewById(R.id.single_result_info);

        Intent i = getIntent();

        if (getIntent().getParcelableExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT) != null) {

            thisCourseAttempt = i.getParcelableExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT);
            displayCourseResult();

        } else {


        }


    }

    public class SplitsListAdapter extends BaseAdapter implements ListAdapter {
        private ValidatedCourseAttempt mValidatedCourseAttempt;
        private Context context;


        private int listLength;

        public SplitsListAdapter(ValidatedCourseAttempt mValidatedCourseAttempt, Context context) {

            this.mValidatedCourseAttempt = mValidatedCourseAttempt;
            this.context = context;
            this.listLength = mValidatedCourseAttempt.sortedValidatedCPTs.size();
        }


        @Override
        public int getCount() {
            return mValidatedCourseAttempt.sortedValidatedCPTs.size();
        }

        @Override
        public Object getItem(int pos) {
            return mValidatedCourseAttempt.sortedValidatedCPTs.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
            //just return 0 if your list items do not have an Id variable.
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (mValidatedCourseAttempt.courseAttempt.course.isScore) {

                return getScoreView(position, convertView, parent);
            } else {

                return getEndToEndView(position, convertView, parent);
            }

        }

        private View getEndToEndView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.splits_list_item, null);
            }

            TextView legNumberView = (TextView) view.findViewById(R.id.qr_list_checkpoint_number);
            TextView listItemSplitTimeView = (TextView) view.findViewById(R.id.qr_list_split_time);
            TextView listItemCumulativeTimeView = (TextView) view.findViewById(R.id.qr_list_cumulative_time);

            CheckpointTime mCpt = mValidatedCourseAttempt.sortedValidatedCPTs.get(position);

            if (position > 0) {

                if (mCpt.checkpointTime == null) {

                    listItemCumulativeTimeView.setText("No Time");
                    listItemSplitTimeView.setText("No Split");

                } else {

                    listItemCumulativeTimeView.setText(CheckpointTime.timeLongToChronoString(
                            mCpt.checkpointTime.getMillis() - mValidatedCourseAttempt.sortedValidatedCPTs.get(0).checkpointTime.getMillis()
                    ));

                    CheckpointTime mLastCpt = mValidatedCourseAttempt.sortedValidatedCPTs.get(position - 1);

                    if (mLastCpt.checkpointTime != null) {

                        listItemSplitTimeView.setText(
                                CheckpointTime.timeLongToChronoString(
                                        mCpt.checkpointTime.getMillis() - mLastCpt.checkpointTime.getMillis()
                                )
                        );

                    } else {

                        listItemSplitTimeView.setText("No Split");
                    }

                }

            } else {

                listItemCumulativeTimeView.setText(CheckpointTime.timeLongToChronoString(0));
                listItemSplitTimeView.setText("Start");
            }


            //Assign leg number
            String legNumber = String.valueOf(position);

            if (position == listLength - 1) {

                legNumber = "F";

            } else if (position == 0) {

                legNumber = "S";
            }

            legNumberView.setText(legNumber);


            switch (mCpt.cptStatus) {

                case CheckpointTime.CORRECT_CPT:
                    legNumberView.setBackgroundResource(R.drawable.border_curved_green);
                    listItemSplitTimeView.setBackgroundResource(R.drawable.border_curved_green);
                    listItemCumulativeTimeView.setBackgroundResource(R.drawable.border_curved_green);
                    break;
                case CheckpointTime.WRONG_ORDER_CPT:
                    legNumberView.setBackgroundResource(R.drawable.border_curved_amber);
                    listItemSplitTimeView.setBackgroundResource(R.drawable.border_curved_amber);
                    listItemCumulativeTimeView.setBackgroundResource(R.drawable.border_curved_amber);
                    break;
                case CheckpointTime.MISSING_CPT:
                    legNumberView.setBackgroundResource(R.drawable.border_curved_red);
                    listItemSplitTimeView.setBackgroundResource(R.drawable.border_curved_red);
                    listItemCumulativeTimeView.setBackgroundResource(R.drawable.border_curved_red);
                    break;

            }


            return view;

        }

        private View getScoreView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.splits_list_item, null);
            }

            TextView legNumberView = (TextView) view.findViewById(R.id.qr_list_checkpoint_number);
            TextView listItemSplitTimeView = (TextView) view.findViewById(R.id.qr_list_split_time);
            TextView listItemCumulativeTimeView = (TextView) view.findViewById(R.id.qr_list_cumulative_time);

            CheckpointTime mCpt = mValidatedCourseAttempt.sortedValidatedCPTs.get(position);

            if (position > 0) {

                if (mCpt.checkpointTime == null) {

                    listItemCumulativeTimeView.setText("No Time");
                    listItemSplitTimeView.setText("");

                } else {

                    listItemCumulativeTimeView.setText(CheckpointTime.timeLongToChronoString(
                            mCpt.checkpointTime.getMillis() - mValidatedCourseAttempt.sortedValidatedCPTs.get(0).checkpointTime.getMillis()
                    ));


                    listItemSplitTimeView.setText("+" + String.valueOf(mValidatedCourseAttempt.courseAttempt.course.checkpointPointsMap.get(mCpt.checkpointID)));


                }

            } else {

                listItemCumulativeTimeView.setText(CheckpointTime.timeLongToChronoString(0));
                listItemSplitTimeView.setText("");
            }


            //Assign leg number
            String legNumber;

            if (position == listLength - 1) {

                if (mCpt.checkpointTime == null) {

                    listItemSplitTimeView.setText("No Time");
                    listItemCumulativeTimeView.setText("");

                } else {

                    listItemSplitTimeView.setText(CheckpointTime.timeLongToChronoString(
                            mCpt.checkpointTime.getMillis() - mValidatedCourseAttempt.sortedValidatedCPTs.get(0).checkpointTime.getMillis()
                    ));


                    listItemCumulativeTimeView.setText("-" + String.valueOf(mValidatedCourseAttempt.scorePenalty));


                }

                legNumber = "F";

            } else if (position == 0) {

                legNumber = "S";

                listItemSplitTimeView.setText(CheckpointTime.timeLongToChronoString(0));
                listItemCumulativeTimeView.setText("");
            } else {

                if (mCpt.checkpointTime == null) {

                    listItemSplitTimeView.setText("No Time");
                    listItemCumulativeTimeView.setText("");

                } else {

                    listItemSplitTimeView.setText(CheckpointTime.timeLongToChronoString(
                            mCpt.checkpointTime.getMillis() - mValidatedCourseAttempt.sortedValidatedCPTs.get(0).checkpointTime.getMillis()
                    ));


                    listItemCumulativeTimeView.setText("+" + String.valueOf(mValidatedCourseAttempt.courseAttempt.course.checkpointPointsMap.get(mCpt.checkpointID)));


                }

                legNumber = mCpt.checkpointID;

            }

            legNumberView.setText(legNumber);

            legNumberView.setBackgroundResource(R.drawable.border_curved_green);
            listItemSplitTimeView.setBackgroundResource(R.drawable.border_curved_green);
            listItemCumulativeTimeView.setBackgroundResource(R.drawable.border_curved_green);


            return view;

        }
    }

    private void displayCourseResult() {

        if (thisCourseAttempt.course.isScore) {

            thisCourseAttempt.course.populatePointsMap();
        }

        ValidatedCourseAttempt vca = thisCourseAttempt.validateAttempt();

        SplitsListAdapter displaySplitsListAdapter = new SplitsListAdapter(vca, this);

        splitsView.setAdapter(displaySplitsListAdapter);

        displaySplitsListAdapter.notifyDataSetChanged();

        courseResultInfoView.setText(vca.getResultInfoString());


        if (vca.status == ValidatedCourseAttempt.STATUS_RTD) {

            courseResultTimeView.setText("RTD.");

        } else if (vca.status == ValidatedCourseAttempt.STATUS_DSQ) {

            courseResultTimeView.setText("DSQ.");
        } else {

            if (thisCourseAttempt.course.isScore) {

                courseResultTimeView.setText(String.valueOf(vca.scorePoints - vca.scorePenalty) + " pts");

            } else {

                courseResultTimeView.setText(CheckpointTime.timeLongToChronoString(vca.getCourseTime()));

            }


        }

    }


}
