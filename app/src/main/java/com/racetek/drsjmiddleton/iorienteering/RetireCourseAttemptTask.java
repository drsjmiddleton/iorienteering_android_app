package com.racetek.drsjmiddleton.iorienteering;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteDbFromObjectHelper;
import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteIncorrectObjectForDbException;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseParams;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by drsjmiddleton on 01/11/2016.
 */
public class RetireCourseAttemptTask extends AsyncTask<DatabaseParams, Void, Void> {


    public RetireCourseAttemptTask() {
    }

    @Override
    protected Void doInBackground(DatabaseParams... params) {

        AutoSQLiteDbFromObjectHelper dbHelper = new AutoSQLiteDbFromObjectHelper(params[0].context, new ArrayList<Class>(Collections.singletonList(CourseAttempt.class)), StaticProjectValues.NAME_OF_COURSE_ATTEMPT_DATABASE);

        SQLiteDatabase courseAttemptDb = dbHelper.getWritableDatabase();

        CourseAttempt mCourseAttempt;

        try {

            mCourseAttempt = (CourseAttempt) dbHelper.getObjectFromDb(params[0].objectPrimaryKey, CourseAttempt.class);
            if (mCourseAttempt != null) {

                mCourseAttempt.retired = true;
                dbHelper.insertObjectIntoDb(mCourseAttempt);

            }

        } catch (AutoSQLiteIncorrectObjectForDbException e) {

        } catch (SQLException e) {

            return null;
        }

        return null;
    }

}
