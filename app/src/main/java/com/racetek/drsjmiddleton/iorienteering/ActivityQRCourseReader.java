package com.racetek.drsjmiddleton.iorienteering;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.autosqlite.drsjmiddleton.autosqlite.DatabaseInserter;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseLoader;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseParams;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ActivityQRCourseReader extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    private static final long SCREEN_PAUSE = 3000;

    private static final int STATUS_AW_SETUP = 0;
    private static final int STATUS_AW_START = 1;
    private static final int STATUS_AW_DIB = 2;
    private static final int STATUS_JUST_DIBBED = 3;
    private static final int STATUS_JUST_DIBBED_ERROR = 4;
    private static final int STATUS_FINISHED_RETIRED = 5;


    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */

    private SurfaceView mCameraView;
    private RelativeLayout infoOverlayView;
    private LinearLayout resultsView;
    private TextView courseResultTimeView;
    private ListView courseSplitsView;


    ImageView statusLED1;
    ImageView statusLED2;
    ImageView statusLED3;
    ImageView statusLED4;

    TextView nextCodeTextView;


    private TextView controlCodeTextView;
    private LinearLayout codeView;
    private Chronometer courseTime;
    private Chronometer legTime;


    private BarcodeDetector barcodeDetector;

    private CameraSource cameraSource;
    private CameraSource.PictureCallback cameraPictureCallback;

    private DisplayMetrics displayMetrics;

    MediaPlayer beepPlayer;

    boolean blockReader = false;

    ArrayList<CheckpointTime> checkpointTimeList;

    CourseAttempt thisCourseAttempt;
    Context activityContext;

    DatabaseLoader courseAttemptDBLoader;

    String username;


    int currentStatus = 0;

    int secondsToWholeScorePoint;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor spEditor;


    ArrayList<String> checkpointIdsList;

    Handler dibHandler;

    long currentElapsedCourseTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.qrreader2_base);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        dibHandler = new Handler(Looper.getMainLooper()) {

            @Override
            public void handleMessage(Message inputMessage) {

                String dibString = (String) inputMessage.getData().getCharSequence(BUNDLE_QR_STRING, "");

                if (dibString.length() > 0) {

                    //beepPlayer.start();

                    processDib(dibString);

                }

            }
        };

        activityContext = this;
        sharedPreferences = getSharedPreferences(StaticProjectValues.SHARED_PREFS, Context.MODE_PRIVATE);
        spEditor = sharedPreferences.edit();

        Intent i = getIntent();

        // set instance variables


        username = sharedPreferences.getString(StaticProjectValues.SHARED_PREFS_USERNAME, "");
        checkpointTimeList = new ArrayList<>();
        checkpointIdsList = new ArrayList<>();


        //top level views
        mCameraView = (SurfaceView) findViewById(R.id.qr_camera_view);
        infoOverlayView = (RelativeLayout) findViewById(R.id.qr_info_overlay);
        resultsView = (LinearLayout) findViewById(R.id.qr_course_results_view);
        resultsView.setVisibility(View.GONE);


        //Results views
        courseResultTimeView = (TextView) findViewById(R.id.qr_course_results_time);
        courseSplitsView = (ListView) findViewById(R.id.qr_course_results_splits_list);


        //qrContents = (TextView) findViewById(R.id.qr_qr_text);
        controlCodeTextView = (TextView) findViewById(R.id.qr_control_code);
        codeView = (LinearLayout) findViewById(R.id.qr_control_code_layout);
        codeView.setVisibility(View.GONE);


        nextCodeTextView = (TextView) findViewById(R.id.qr_next_code);

        statusLED1 = (ImageView) findViewById(R.id.qr_statusLed1);
        statusLED2 = (ImageView) findViewById(R.id.qr_statusLed2);
        statusLED3 = (ImageView) findViewById(R.id.qr_statusLed3);
        statusLED4 = (ImageView) findViewById(R.id.qr_statusLed4);

        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);


        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(displayMetrics.widthPixels, displayMetrics.heightPixels)
                .setAutoFocusEnabled(true)
                .setRequestedFps(15.0f)
                .build();


        mCameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {

                try {
                    cameraSource.start(mCameraView.getHolder());
                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {

                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (!blockReader) {
                    if (barcodes.size() != 0) {

                        blockReader = true;

                        Message dibmessage = dibHandler.obtainMessage();

                        Bundle dibBundle = new Bundle();

                        dibBundle.putCharSequence(BUNDLE_QR_STRING, barcodes.valueAt(0).displayValue);

                        dibmessage.setData(dibBundle);

                        dibmessage.sendToTarget();

                    }
                }
            }
        });

        beepPlayer = MediaPlayer.create(this, R.raw.tag_arrived_tone);
        beepPlayer.setLooping(false);

        courseTime = (Chronometer) findViewById(R.id.qr_race_time);
        courseTime.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer cArg) {
                currentElapsedCourseTime = DateTime.now().getMillis() - cArg.getBase();
                cArg.setText(CheckpointTime.timeLongToChronoString(currentElapsedCourseTime));
                if (thisCourseAttempt != null && thisCourseAttempt.course.isScore) {

                    int wholeSecondsToPoint = thisCourseAttempt.course.getSecondsToWholeScorePoint();

                    if (Math.abs(currentElapsedCourseTime % (1000 * wholeSecondsToPoint)) < 2000) {

                        nextCodeTextView.setText(String.valueOf(calculateCurrentPoints()));


                    }

                }
            }
        });

        legTime = (Chronometer) findViewById(R.id.qr_leg_time);
        legTime.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = DateTime.now().getMillis() - cArg.getBase();
                cArg.setText(CheckpointTime.timeLongToChronoString(time));
            }
        });

        if (i.getParcelableExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT) != null) {

            thisCourseAttempt = i.getParcelableExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT);
            setupWithCourseAttempt();

        } else {


            setupAwaitingInfoScan();

        }
    }

    @Override
    public void onDestroy() {

        beepPlayer.stop();


        super.onDestroy();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();
    }


    private void toggleLEDs(int status) {


        switch (status) {

            case STATUS_AW_START:
                statusLED1.setImageResource(R.drawable.red_led);
                statusLED2.setImageResource(R.drawable.red_led);
                statusLED3.setImageResource(R.drawable.red_led);
                statusLED4.setImageResource(R.drawable.red_led);
                break;
            case STATUS_AW_DIB:
                statusLED1.setImageResource(R.drawable.green_led);
                statusLED2.setImageResource(R.drawable.green_led);
                statusLED3.setImageResource(R.drawable.green_led);
                statusLED4.setImageResource(R.drawable.green_led);
                break;
            case STATUS_JUST_DIBBED:
                statusLED1.setImageResource(R.drawable.amber_led);
                statusLED2.setImageResource(R.drawable.amber_led);
                statusLED3.setImageResource(R.drawable.amber_led);
                statusLED4.setImageResource(R.drawable.amber_led);
                break;
            case STATUS_AW_SETUP:
                statusLED1.setImageResource(R.drawable.blue_led);
                statusLED2.setImageResource(R.drawable.blue_led);
                statusLED3.setImageResource(R.drawable.blue_led);
                statusLED4.setImageResource(R.drawable.blue_led);
                break;

            case STATUS_JUST_DIBBED_ERROR:
                statusLED1.setImageResource(R.drawable.red_led);
                statusLED2.setImageResource(R.drawable.red_led);
                statusLED3.setImageResource(R.drawable.red_led);
                statusLED4.setImageResource(R.drawable.red_led);
                break;
            case STATUS_FINISHED_RETIRED:
                statusLED1.setImageResource(R.drawable.red_led);
                statusLED2.setImageResource(R.drawable.red_led);
                statusLED3.setImageResource(R.drawable.red_led);
                statusLED4.setImageResource(R.drawable.red_led);
                break;

            default:
                statusLED1.setImageResource(R.drawable.red_led);
                statusLED2.setImageResource(R.drawable.red_led);
                statusLED3.setImageResource(R.drawable.red_led);
                statusLED4.setImageResource(R.drawable.red_led);
                break;

        }

    }

    private void changeStatusAfterDib(String codeViewString, int statusDuring, final int statusAfter) {

        final int mStatusAfter = statusAfter;
        final int mStatusDuring = statusAfter;
        final String mcodeViewString = codeViewString;




        codeView.setVisibility(View.VISIBLE);
        toggleLEDs(mStatusDuring);
        controlCodeTextView.setText(mcodeViewString);

        toggleLEDs(statusDuring);

        Timer timer = new Timer(true);

        timer.schedule(new TimerTask() {

            public void run() {

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        codeView.setVisibility(View.GONE);
                        toggleLEDs(mStatusAfter);
                        currentStatus = statusAfter;
                        blockReader = false;

                        if (currentStatus == STATUS_AW_START) {

                            codeView.setVisibility(View.VISIBLE);
                            controlCodeTextView.setText("Go To Start");
                        }
                    }
                });

            }

        }, SCREEN_PAUSE);


    }

    private void processDib(String QRString) {


        String workingString = QRString;

        beepPlayer.start();

        if (currentStatus == STATUS_FINISHED_RETIRED) {
            changeStatusAfterDib("Course Finished.", STATUS_JUST_DIBBED, STATUS_FINISHED_RETIRED);
            return;
        }

        if (workingString.startsWith(StaticProjectValues.URL_PREFIX)) {

            workingString = workingString.substring(StaticProjectValues.URL_PREFIX.length());

            if (workingString.startsWith(StaticProjectValues.SETUP_CODE)) {

                workingString = workingString.substring(StaticProjectValues.SETUP_CODE.length());
                recordCourseSetup(workingString);
                displayNextControlCode();


            } else {

                logDib(workingString);


            }

        }

        //show temporary error message
        else {

            changeStatusAfterDib("Wrong QR Type", STATUS_JUST_DIBBED_ERROR, currentStatus);
        }


    }

    private void logDib(String controlCode) {

        final String mControlCode = controlCode;

        if (currentStatus == STATUS_AW_SETUP) {

            changeStatusAfterDib("Course Setup Required", STATUS_JUST_DIBBED_ERROR, currentStatus);
            return;
        }

        if (currentStatus == STATUS_AW_START) {


            if (controlCode.equals(checkpointIdsList.get(0))) {


                changeStatusAfterDib("GO!", STATUS_JUST_DIBBED, STATUS_AW_DIB);
                recordNewCheckpointTime(controlCode);
                startMainClock();
                resetLegTime();
                displayNextControlCode();
                insertCourseAttemptIntoDB(thisCourseAttempt);
                insertIntoSP(thisCourseAttempt);
                return;

            } else {

                changeStatusAfterDib("Not the start checkpoint.\nGo To Start", STATUS_JUST_DIBBED_ERROR, STATUS_AW_START);
                return;

            }

        }

        if (currentStatus == STATUS_AW_DIB) {


            if (controlCode.equals(checkpointIdsList.get(checkpointIdsList.size() - 1))) {

                recordNewCheckpointTime(controlCode);
                changeStatusAfterDib("Finished!", STATUS_JUST_DIBBED, STATUS_FINISHED_RETIRED);
                thisCourseAttempt.finished = true;
                finishCourse(false);
                insertCourseAttemptIntoDB(thisCourseAttempt);
                return;

            } else {

                changeStatusAfterDib(controlCode, STATUS_JUST_DIBBED, STATUS_AW_DIB);
                resetLegTime();
                recordNewCheckpointTime(controlCode);
                displayNextControlCode();
                insertCourseAttemptIntoDB(thisCourseAttempt);
                return;

            }

        }

    }

    private void recordNewCheckpointTime(String controlCode) {

        CheckpointTime tempCPT = new CheckpointTime(
                String.valueOf(DateTime.now().getMillis()),
                thisCourseAttempt.course.pk,
                controlCode,
                thisCourseAttempt.checkpointTimes.size() + 1,
                DateTime.now());

        thisCourseAttempt.checkpointTimes.add(tempCPT);
        Collections.sort(thisCourseAttempt.checkpointTimes, new CheckpointTime.cptOrderComparator());

    }

    private void recordCourseSetup(String setupString) {

        if (currentStatus != STATUS_AW_SETUP) {

            changeStatusAfterDib("Already Setup", STATUS_JUST_DIBBED_ERROR, currentStatus);
            return;

        }

        final String mSetupString = setupString;

        ObjectMapper mapper = new ObjectMapper();
        TypeFactory typeFactory = mapper.getTypeFactory();

        Course tempCourse = null;

        try {
            tempCourse = mapper.readValue(setupString, typeFactory.constructType(Course.class));
        } catch (Exception e) {


            e.printStackTrace();
            return;

        }

        tempCourse.dateCreated = DateTime.now();

        insertCourseIntoDB(tempCourse);

        thisCourseAttempt = new CourseAttempt(String.valueOf(DateTime.now().getMillis()), tempCourse, username, DateTime.now(), new ArrayList<CheckpointTime>());


        if (thisCourseAttempt.course.isScore) {

            thisCourseAttempt.course.populatePointsMap();
        }


        checkpointIdsList = Lists.newArrayList(
                Splitter.fixedLength(3)
                        .split(thisCourseAttempt.course.courseCodes));

        //insertCourseAttemptIntoDBandSP(thisCourseAttempt);

        changeStatusAfterDib("Course Setup done.", STATUS_JUST_DIBBED, STATUS_AW_START);

    }

    private void displayNextControlCode() {

        if (!thisCourseAttempt.course.isScore) {

            nextCodeTextView.setText(thisCourseAttempt.getNextCode());

        } else {


            nextCodeTextView.setText(String.valueOf(calculateCurrentPoints()));
        }


    }

    private void startMainClock() {

        DateTime startTime = thisCourseAttempt.getStartTime();

        if (startTime == null) {
            courseTime.stop();
            courseTime.setText("00:00:00");
            return;
        } else {
            courseTime.setBase(startTime.getMillis());
            courseTime.start();

        }

    }

    private void resetLegTime() {

        DateTime legStartTime = thisCourseAttempt.getLastControlTime();

        legTime.stop();

        if (legStartTime == null) {

            legTime.setText("00:00:00");
            return;
        } else {

            legTime.setBase(legStartTime.getMillis());
            legTime.start();
        }

    }

    private void finishCourse(boolean retired) {


        if (retired) {

            thisCourseAttempt.retired = true;
        } else {
            thisCourseAttempt.finished = true;

        }


        spEditor.putString(StaticProjectValues.SHARED_PREFS_PK_OF_CURRENT_ATTEMPT, null);
        spEditor.commit();

        legTime.stop();
        legTime.setText("00:00:00");

        courseTime.stop();

        nextCodeTextView.setText("");

        displayCourseResult();

        attemptUpload();


    }

    private void displayCourseResult() {

        ValidatedCourseAttempt vca = thisCourseAttempt.validateAttempt();

        vca.generateXML();

        SplitsListAdapter displaySplitsListAdapter = new SplitsListAdapter(vca, activityContext);

        courseSplitsView.setAdapter(displaySplitsListAdapter);

        displaySplitsListAdapter.notifyDataSetChanged();


        mCameraView.setVisibility(View.GONE);
        infoOverlayView.setVisibility(View.GONE);

        resultsView.setVisibility(View.VISIBLE);

        if (vca.status == ValidatedCourseAttempt.STATUS_RTD) {

            courseResultTimeView.setText("RTD.");

        } else if (vca.status == ValidatedCourseAttempt.STATUS_DSQ) {

            courseResultTimeView.setText("DSQ.");

        } else {

            if (thisCourseAttempt.course.isScore) {

                courseResultTimeView.setText(String.valueOf(vca.scorePoints - vca.scorePenalty) + " pts");

            } else {

                courseResultTimeView.setText(CheckpointTime.timeLongToChronoString(vca.getCourseTime()));

            }


        }

    }

    public class SplitsListAdapter extends BaseAdapter implements ListAdapter {
        private ValidatedCourseAttempt mValidatedCourseAttempt;
        private Context context;


        private int listLength;

        public SplitsListAdapter(ValidatedCourseAttempt mValidatedCourseAttempt, Context context) {

            this.mValidatedCourseAttempt = mValidatedCourseAttempt;
            this.context = context;
            this.listLength = mValidatedCourseAttempt.sortedValidatedCPTs.size();
        }


        @Override
        public int getCount() {
            return mValidatedCourseAttempt.sortedValidatedCPTs.size();
        }

        @Override
        public Object getItem(int pos) {
            return mValidatedCourseAttempt.sortedValidatedCPTs.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
            //just return 0 if your list items do not have an Id variable.
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (mValidatedCourseAttempt.courseAttempt.course.isScore) {

                return getScoreView(position, convertView, parent);
            } else {

                return getEndToEndView(position, convertView, parent);
            }

        }

        private View getEndToEndView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.splits_list_item, null);
            }

            TextView legNumberView = (TextView) view.findViewById(R.id.qr_list_checkpoint_number);
            TextView listItemSplitTimeView = (TextView) view.findViewById(R.id.qr_list_split_time);
            TextView listItemCumulativeTimeView = (TextView) view.findViewById(R.id.qr_list_cumulative_time);

            CheckpointTime mCpt = mValidatedCourseAttempt.sortedValidatedCPTs.get(position);

            if (position > 0) {

                if (mCpt.checkpointTime == null) {

                    listItemCumulativeTimeView.setText("No Time");
                    listItemSplitTimeView.setText("No Split");

                } else {

                    listItemCumulativeTimeView.setText(CheckpointTime.timeLongToChronoString(
                            mCpt.checkpointTime.getMillis() - mValidatedCourseAttempt.sortedValidatedCPTs.get(0).checkpointTime.getMillis()
                    ));

                    CheckpointTime mLastCpt = mValidatedCourseAttempt.sortedValidatedCPTs.get(position - 1);

                    if (mLastCpt.checkpointTime != null) {

                        listItemSplitTimeView.setText(
                                CheckpointTime.timeLongToChronoString(
                                        mCpt.checkpointTime.getMillis() - mLastCpt.checkpointTime.getMillis()
                                )
                        );

                    } else {

                        listItemSplitTimeView.setText("No Split");
                    }

                }

            } else {

                listItemCumulativeTimeView.setText(CheckpointTime.timeLongToChronoString(0));
                listItemSplitTimeView.setText("Start");
            }


            //Assign leg number
            String legNumber = String.valueOf(position);

            if (position == listLength - 1) {

                legNumber = "F";

            } else if (position == 0) {

                legNumber = "S";
            }

            legNumberView.setText(legNumber);


            switch (mCpt.cptStatus) {

                case CheckpointTime.CORRECT_CPT:
                    legNumberView.setBackgroundResource(R.drawable.border_curved_green);
                    listItemSplitTimeView.setBackgroundResource(R.drawable.border_curved_green);
                    listItemCumulativeTimeView.setBackgroundResource(R.drawable.border_curved_green);
                    break;
                case CheckpointTime.WRONG_ORDER_CPT:
                    legNumberView.setBackgroundResource(R.drawable.border_curved_amber);
                    listItemSplitTimeView.setBackgroundResource(R.drawable.border_curved_amber);
                    listItemCumulativeTimeView.setBackgroundResource(R.drawable.border_curved_amber);
                    break;
                case CheckpointTime.MISSING_CPT:
                    legNumberView.setBackgroundResource(R.drawable.border_curved_red);
                    listItemSplitTimeView.setBackgroundResource(R.drawable.border_curved_red);
                    listItemCumulativeTimeView.setBackgroundResource(R.drawable.border_curved_red);
                    break;

            }


            return view;

        }

        private View getScoreView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.splits_list_item, null);
            }

            TextView legNumberView = (TextView) view.findViewById(R.id.qr_list_checkpoint_number);
            TextView listItemSplitTimeView = (TextView) view.findViewById(R.id.qr_list_split_time);
            TextView listItemCumulativeTimeView = (TextView) view.findViewById(R.id.qr_list_cumulative_time);

            CheckpointTime mCpt = mValidatedCourseAttempt.sortedValidatedCPTs.get(position);

            if (position > 0) {

                if (mCpt.checkpointTime == null) {

                    listItemCumulativeTimeView.setText("No Time");
                    listItemSplitTimeView.setText("");

                } else {

                    listItemCumulativeTimeView.setText(CheckpointTime.timeLongToChronoString(
                            mCpt.checkpointTime.getMillis() - mValidatedCourseAttempt.sortedValidatedCPTs.get(0).checkpointTime.getMillis()
                    ));


                    listItemSplitTimeView.setText("+" + String.valueOf(mValidatedCourseAttempt.courseAttempt.course.checkpointPointsMap.get(mCpt.checkpointID)));


                }

            } else {

                listItemCumulativeTimeView.setText(CheckpointTime.timeLongToChronoString(0));
                listItemSplitTimeView.setText("");
            }


            //Assign leg number
            String legNumber;

            if (position == listLength - 1) {

                if (mCpt.checkpointTime == null) {

                    listItemSplitTimeView.setText("No Time");
                    listItemCumulativeTimeView.setText("");

                } else {

                    listItemSplitTimeView.setText(CheckpointTime.timeLongToChronoString(
                            mCpt.checkpointTime.getMillis() - mValidatedCourseAttempt.sortedValidatedCPTs.get(0).checkpointTime.getMillis()
                    ));


                    listItemCumulativeTimeView.setText("-" + String.valueOf(mValidatedCourseAttempt.scorePenalty));


                }

                legNumber = "F";

            } else if (position == 0) {

                legNumber = "S";

                listItemSplitTimeView.setText(CheckpointTime.timeLongToChronoString(0));
                listItemCumulativeTimeView.setText("");
            } else {

                if (mCpt.checkpointTime == null) {

                    listItemSplitTimeView.setText("No Time");
                    listItemCumulativeTimeView.setText("");

                } else {

                    listItemSplitTimeView.setText(CheckpointTime.timeLongToChronoString(
                            mCpt.checkpointTime.getMillis() - mValidatedCourseAttempt.sortedValidatedCPTs.get(0).checkpointTime.getMillis()
                    ));


                    listItemCumulativeTimeView.setText("+" + String.valueOf(mValidatedCourseAttempt.courseAttempt.course.checkpointPointsMap.get(mCpt.checkpointID)));


                }

                legNumber = mCpt.checkpointID;

            }

            legNumberView.setText(legNumber);

            legNumberView.setBackgroundResource(R.drawable.border_curved_green);
            listItemSplitTimeView.setBackgroundResource(R.drawable.border_curved_green);
            listItemCumulativeTimeView.setBackgroundResource(R.drawable.border_curved_green);


            return view;

        }
    }

    private void insertCourseIntoDB(Course course) {

        new DatabaseInserter().execute(new DatabaseParams(
                Course.class, StaticProjectValues.NAME_OF_COURSE_DATABASE, activityContext, course));

    }

    private void insertIntoSP(CourseAttempt courseAttempt) {

        if (courseAttempt == null) {

            return;
        }

        String storedPK = sharedPreferences.getString(StaticProjectValues.SHARED_PREFS_PK_OF_CURRENT_ATTEMPT, null);

        if (storedPK != null) {

            if (storedPK.equals(courseAttempt.pk)) {
                return;
            } else {
                new RetireCourseAttemptTask().execute(
                        new DatabaseParams(0,
                                CourseAttempt.class,
                                StaticProjectValues.NAME_OF_COURSE_ATTEMPT_DATABASE,
                                activityContext,
                                storedPK)

                );

            }

        }

        spEditor.putString(StaticProjectValues.SHARED_PREFS_PK_OF_CURRENT_ATTEMPT, courseAttempt.pk);
        spEditor.commit();
    }

    private void insertCourseAttemptIntoDB(CourseAttempt courseAttempt) {

        if (courseAttempt == null) {

            return;
        }

        if (courseAttempt.finished || courseAttempt.retired) {

            spEditor.putString(StaticProjectValues.SHARED_PREFS_PK_OF_CURRENT_ATTEMPT, null);
            spEditor.commit();

        }

        new DatabaseInserter().execute(new DatabaseParams(
                CourseAttempt.class, StaticProjectValues.NAME_OF_COURSE_ATTEMPT_DATABASE, activityContext, courseAttempt));

    }

    private void setupWithCourseAttempt() {

        checkpointIdsList = Lists.newArrayList(
                Splitter.fixedLength(3)
                        .split(thisCourseAttempt.course.courseCodes));


        if (thisCourseAttempt.course.isScore) {

            thisCourseAttempt.course.populatePointsMap();
        }

        if (thisCourseAttempt.getStartTime() == null) {
            toggleLEDs(STATUS_AW_START);
            currentStatus = STATUS_AW_START;
            blockReader = false;
            codeView.setVisibility(View.VISIBLE);
            changeStatusAfterDib("Course Setup done.", STATUS_JUST_DIBBED, STATUS_AW_START);
            displayNextControlCode();

        } else {

            toggleLEDs(STATUS_AW_START);
            currentStatus = STATUS_AW_DIB;
            blockReader = false;
            codeView.setVisibility(View.GONE);
            displayNextControlCode();
            startMainClock();
            resetLegTime();

        }


    }

    private void setupAwaitingInfoScan() {


        toggleLEDs(STATUS_AW_SETUP);
        currentStatus = STATUS_AW_SETUP;
        blockReader = false;
        codeView.setVisibility(View.VISIBLE);
        controlCodeTextView.setText(getResources().getString(R.string.qr_screen_course_message_go_to_setup));


    }

    private int calculateCurrentPoints() {


        int pointsCollected = thisCourseAttempt.getCurrentPointsTotal();

        if (currentElapsedCourseTime > thisCourseAttempt.course.getTimeLimit() * 60 * 1000) {

            int overdue = (int) (currentElapsedCourseTime - thisCourseAttempt.course.getTimeLimit() * 60 * 1000) / 1000;

            int penalty = (int) Math.ceil(overdue * thisCourseAttempt.course.penalty);

            return pointsCollected - penalty;


        } else {

            return pointsCollected;

        }


    }

    private void attemptUpload() {

        Intent startUploadIntent = new Intent(this, ServiceResultSender.class);

        startUploadIntent.putExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT, thisCourseAttempt);

        startService(startUploadIntent);

    }

    private final String BUNDLE_QR_STRING = "**BUNDLE_QR_STRING**";
}
