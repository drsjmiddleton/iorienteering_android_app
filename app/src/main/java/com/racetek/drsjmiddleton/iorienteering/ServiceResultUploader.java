package com.racetek.drsjmiddleton.iorienteering;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class ServiceResultUploader extends IntentService {

    String token;
    SharedPreferences sharedpreferences;
    private OkHttpClient HTTPClient;


    public ServiceResultUploader() {
        super("ServiceResultUploader");
    }

    @Override
    public void onCreate() {


        sharedpreferences = getSharedPreferences(StaticProjectValues.SHARED_PREFS, Context.MODE_PRIVATE);


        super.onCreate();
    }


    @Override
    protected void onHandleIntent(Intent i) {


    }

}
