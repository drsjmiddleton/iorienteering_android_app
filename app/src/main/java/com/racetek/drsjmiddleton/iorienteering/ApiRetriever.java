package com.racetek.drsjmiddleton.iorienteering;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by drsjmiddleton on 08/05/2015.
 * <p/>
 * SHOULD NOT BE INSTANTIATED ON MAIN THREAD
 */
public class ApiRetriever<T> {


    String uri;
    String responseString;
    int responseCode;
    ArrayList<NameValuePair> headerSet;
    ArrayList<NameValuePair> parameterSet;
    int sendAttemptCounter;
    boolean retrievalSuccessful = false;
    String token;
    int function;
    int timeout;
    public ArrayList<T> returnList;
    public Class<T> typeParameterClass;


    private OkHttpClient HTTPClient;

    final int[] ACCEPTABLE_CODES = {200, 202, 206, 403, 404, 406, 409, 410, 412};

    public ApiRetriever(String uri, ArrayList<NameValuePair> headerSet, ArrayList<NameValuePair> parameterSet, String token, int function) {

        this.uri = uri;
        this.headerSet = headerSet;
        this.parameterSet = parameterSet;
        this.token = token;
        this.function = function;
        this.timeout = 10;

        HTTPClient = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .build();

        getApiResponse();

    }

    public ApiRetriever(String uri, ArrayList<NameValuePair> headerSet, ArrayList<NameValuePair> parameterSet, String token, int function, Class<T> typeClass) {

        this.uri = uri;
        this.headerSet = headerSet;
        this.parameterSet = parameterSet;
        this.token = token;
        this.function = function;
        this.timeout = 5;
        this.typeParameterClass = typeClass;

        HTTPClient = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .build();

        getApiResponse();

    }

    public ApiRetriever(Object[] params) {
        this.uri = (String) params[1];
        this.headerSet = (ArrayList<NameValuePair>) params[2];
        this.parameterSet = (ArrayList<NameValuePair>) params[3];
        this.token = (String) params[4];
        this.function = (int) params[5];
        this.timeout = 5;
        this.typeParameterClass = (Class<T>) params[6];

        HTTPClient = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .build();

        getApiResponse();

    }

    public ApiRetriever(String uri, ArrayList<NameValuePair> headerSet, ArrayList<NameValuePair> parameterSet, String token, int function, int timeout) {

        this.uri = uri;
        this.headerSet = headerSet;
        this.parameterSet = parameterSet;
        this.token = token;
        this.function = function;
        this.timeout = timeout;

        HTTPClient = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .build();

        getApiResponse();

    }

    public ApiRetriever(String uri, String token, int function) {

        this.uri = uri;

        this.headerSet = new ArrayList<NameValuePair>();
        ;
        this.parameterSet = new ArrayList<NameValuePair>();
        ;
        this.token = token;
        this.function = function;
        this.timeout = 10;

        HTTPClient = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .build();

        getApiResponse();

    }


    public void getApiResponse() {


        Request.Builder requestBuilder = new Request.Builder()
                .url(uri);


        for (NameValuePair nvp : headerSet) {

            requestBuilder.addHeader(nvp.getName(), nvp.getValue());

        }

        Request request;

        switch (function) {
            case GET:
            case GET_JSON_LIST:
                request = requestBuilder.addHeader("Authorization", "Token " + token).get().build();
                break;
            case POST:
                FormBody.Builder formBuilder = new FormBody.Builder();

                for (NameValuePair nvp : parameterSet) {

                    formBuilder.add(nvp.getName(), nvp.getValue());

                }

                request = requestBuilder.addHeader("Authorization", "Token " + token).post(formBuilder.build()).build();
                break;

            default:
                request = requestBuilder.get().build();
                break;

        }

        for (int i = 0; i < 5; i++) {


            try {

                Response response = HTTPClient.newCall(request).execute();

                responseString = response.body().string();

                responseCode = response.code();

                for (int code : ACCEPTABLE_CODES) {

                    if (responseCode == code) {

                        retrievalSuccessful = true;
                        break;
                    }

                }

                if (responseCode == 200) {

                    if (function == GET_JSON_LIST) {

                        ObjectMapper mapper = new ObjectMapper();
                        //mapper.registerModule(new JodaModule());
                        TypeFactory typeFactory = mapper.getTypeFactory();
                        returnList = mapper.readValue(responseString, typeFactory.constructCollectionType(ArrayList.class, typeParameterClass));

                    }

                }

                break;

            } catch (IOException e) {

            } catch (Exception e) {

                System.out.print(e.toString());

            }

        }


    }


    public final static int GET = 1;
    public final static int POST = 3;
    public final static int GET_JSON_LIST = 4;

}
