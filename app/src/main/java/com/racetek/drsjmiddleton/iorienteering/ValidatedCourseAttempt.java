package com.racetek.drsjmiddleton.iorienteering;

import android.os.Parcel;
import android.os.Parcelable;

import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteIgnoreFieldAnnotation;

import org.joda.time.DateTime;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by drsjmiddleton on 03/10/2016.
 */


public class ValidatedCourseAttempt implements Parcelable {


    CourseAttempt courseAttempt;

    ArrayList<CheckpointTime> sortedValidatedCPTs;

    int status = 0;

    String dsqReason;

    int scorePoints;

    int scorePenalty;

    public ValidatedCourseAttempt() {

        //empty constructor for AutoSQLlite

    }

    public ValidatedCourseAttempt(CourseAttempt courseAttempt, int status, ArrayList<CheckpointTime> sortedValidatedCPTs, String dsqReason) {

        this.courseAttempt = courseAttempt;
        this.sortedValidatedCPTs = sortedValidatedCPTs;

        this.status = status;
        this.dsqReason = dsqReason;

        this.scorePoints = 0;
        this.scorePenalty = 0;

    }

    public ValidatedCourseAttempt(CourseAttempt courseAttempt, ArrayList<CheckpointTime> sortedValidatedCPTs, int status, String dsqReason, int scorePoints, int scorePenalty) {
        this.courseAttempt = courseAttempt;
        this.sortedValidatedCPTs = sortedValidatedCPTs;
        this.status = status;
        this.dsqReason = dsqReason;
        this.scorePoints = scorePoints;
        this.scorePenalty = scorePenalty;
    }

    public ValidatedCourseAttempt(Parcel in) {

        this.courseAttempt = in.readParcelable(null);

        this.status = in.readInt();

        in.readTypedList(this.sortedValidatedCPTs, CheckpointTime.CREATOR);

        this.dsqReason = in.readString();

        this.scorePoints = in.readInt();
        this.scorePenalty = in.readInt();

    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeParcelable(courseAttempt, 0);
        out.writeInt(status);
        out.writeTypedList(sortedValidatedCPTs);
        out.writeString(dsqReason);
        out.writeInt(scorePoints);
        out.writeInt(scorePenalty);

    }

    @AutoSQLiteIgnoreFieldAnnotation
    public static final Parcelable.Creator<ValidatedCourseAttempt> CREATOR
            = new Parcelable.Creator<ValidatedCourseAttempt>() {
        public ValidatedCourseAttempt createFromParcel(Parcel in) {
            return new ValidatedCourseAttempt(in);
        }

        public ValidatedCourseAttempt[] newArray(int size) {
            return new ValidatedCourseAttempt[size];
        }
    };

    public DateTime getFinishTime() {

        if (status == STATUS_RTD || status == STATUS_DNF) {

            return null;
        }

        Collections.sort(sortedValidatedCPTs, new CheckpointTime.cptOrderComparator());

        try {
            return sortedValidatedCPTs.get(sortedValidatedCPTs.size() - 1).getCheckpointTime();
        } catch (IndexOutOfBoundsException e) {

            return null;
        }


    }

    public DateTime getLastControlTime() {

        Collections.sort(sortedValidatedCPTs, new CheckpointTime.cptOrderComparator());

        try {
            return sortedValidatedCPTs.get(sortedValidatedCPTs.size() - 1).getCheckpointTime();
        } catch (IndexOutOfBoundsException e) {

            return null;
        }

    }

    public DateTime getStartTime() {

        Collections.sort(sortedValidatedCPTs, new CheckpointTime.cptOrderComparator());

        try {
            return sortedValidatedCPTs.get(0).getCheckpointTime();
        } catch (IndexOutOfBoundsException e) {

            return null;
        }

    }

    public long getCourseTime() {

        if (getFinishTime() == null || getStartTime() == null) {

            return 0;
        } else {
            try {
                return getFinishTime().getMillis() - getStartTime().getMillis();
            } catch (Exception e) {
                return 0;
            }
        }

    }

    public long getOngoingCourseTime() {

        if (getLastControlTime() == null || getStartTime() == null) {

            return 0;
        }

        try {
            return getLastControlTime().getMillis() - getStartTime().getMillis();
        } catch (Exception e) {
            return 0;
        }
    }

    public String getResultInfoString() {

        return "Result for " + this.courseAttempt.course.courseName + " at " + this.courseAttempt.course.areaName + ". (" + this.courseAttempt.attemptTime.toString("dd/MM/yyyy HH:mm:ss") + ")";
    }

    public String generateXML() {

        Serializer serializer = new Persister();

        StringWriter xmlWriter = new StringWriter();

        XMLCourseAttempt xmlCourseAttempt = new XMLCourseAttempt(this);

        try {
            serializer.write(xmlCourseAttempt, xmlWriter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String returnString = xmlWriter.toString();

        return returnString;

    }

    public final static int STATUS_COMPLETED = 0;
    public final static int STATUS_DNF = 1;
    public final static int STATUS_DSQ = 2;
    public final static int STATUS_RTD = 3;
    public final static int STATUS_MISSING = 4;
}
