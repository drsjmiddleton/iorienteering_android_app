package com.racetek.drsjmiddleton.iorienteering;

import android.os.Parcel;
import android.os.Parcelable;

import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteIgnoreFieldAnnotation;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by drsjmiddleton on 01/09/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Area implements Parcelable {

    @JsonProperty("pk")
    String pk;

    @JsonProperty("area_name")
    String areaName;
    @JsonProperty("latitude")
    Double latitude;
    @JsonProperty("longitude")
    Double longitude;

    public Area() {

        //empty constructor for AutoSQLlite

    }

    public Area(Parcel in) {

        this.pk = in.readString();
        this.areaName = in.readString();

        this.latitude = in.readDouble();
        this.longitude = in.readDouble();

    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeString(pk);
        out.writeString(areaName);
        out.writeDouble(latitude);
        out.writeDouble(longitude);

    }

    @AutoSQLiteIgnoreFieldAnnotation
    public static final Parcelable.Creator<Area> CREATOR
            = new Parcelable.Creator<Area>() {
        public Area createFromParcel(Parcel in) {
            return new Area(in);
        }

        public Area[] newArray(int size) {
            return new Area[size];
        }
    };

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public static Area getAreaFromListByName(String areaName, ArrayList<Area> areaList) {

        for (Area mArea : areaList) {

            if (mArea.areaName.equals(areaName)) {


                return mArea;
            }
        }

        return null;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Area area = (Area) o;

        return getPk().equals(area.getPk());

    }

    @Override
    public int hashCode() {
        return getPk().hashCode();
    }
}
