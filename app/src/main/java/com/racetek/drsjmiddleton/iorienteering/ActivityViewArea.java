package com.racetek.drsjmiddleton.iorienteering;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.autosqlite.drsjmiddleton.autosqlite.DatabaseInserter;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseLoader;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseParams;
import com.autosqlite.drsjmiddleton.autosqlite.DbLoaderCallback;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;


public class ActivityViewArea extends AppCompatActivity implements DbLoaderCallback, ApiLoaderCallback {


    final Context thisActivityContext = this;

    Area thisArea;

    SharedPreferences sharedPreferences;

    ListView courseListView;

    ProgressBar mProgressView;

    TextView areaInfoViewtView;

    ArrayList<Course> loadedCourses;
    ArrayList<Course> cloudCourses;
    CourseListAdapter courseListAdapter;

    final Context activityContext = this;

    DatabaseLoader courseDBLoader;
    ApiAsyncTask<Course> courseApiAsyncTask;

    boolean databaseLoaded = false;
    boolean cloudLoaded = false;

    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.view_area_base);

        Toolbar toolbar = (Toolbar) findViewById(R.id.view_area_toolbar);
        //toolbar.setTitle("Home");
        setSupportActionBar(toolbar);

        sharedPreferences = getSharedPreferences(StaticProjectValues.SHARED_PREFS, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(StaticProjectValues.SHARED_PREFS_API_TOKEN, null);

        courseListView = (ListView) findViewById(R.id.view_area_course_list);
        mProgressView = (ProgressBar) findViewById(R.id.view_area_progress);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.view_area_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent startHomeScreenIntent = new Intent(thisActivityContext, ActivityUserHome.class);

                startActivity(startHomeScreenIntent);

            }
        });

        Intent i = getIntent();

        thisArea = i.getParcelableExtra(StaticProjectValues.INTENT_THIS_AREA);

        toolbar.setTitle(thisArea.getAreaName());

        loadedCourses = new ArrayList<>();
        cloudCourses = new ArrayList<>();

        showProgress(true);

        //load course from web and db - show spinner while this is occuring

        courseDBLoader = new DatabaseLoader();


        courseDBLoader.setCallback(this).execute(new Object[]{

                new DatabaseParams(
                        StaticProjectValues.COURSE_DB_LOADER_START_ID,
                        Course.class,
                        StaticProjectValues.NAME_OF_COURSE_DATABASE,
                        this,
                        thisArea.areaName,
                        "areaName")

        });

        if (token != null) {

            String correctUrl = StaticProjectValues.API_GET_AREA_COURSE_LIST.replace("**AREA_PK**", thisArea.pk);

            courseApiAsyncTask = (ApiAsyncTask) new ApiAsyncTask<Course>().setCallback(this).execute(

                    new Object[]{
                            1,
                            correctUrl,
                            new ArrayList<NameValuePair>(),
                            new ArrayList<NameValuePair>(),
                            token,
                            ApiRetriever.GET_JSON_LIST,
                            Course.class
                    }

            );

        }


        courseListAdapter = new CourseListAdapter(cloudCourses, this);

        courseListView.setAdapter(courseListAdapter);


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            courseListView.setVisibility(show ? View.GONE : View.VISIBLE);
            courseListView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    courseListView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            courseListView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    public class CourseListAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<Course> courseList = new ArrayList<Course>();
        private Context context;


        public CourseListAdapter(ArrayList<Course> courseList, Context context) {
            this.courseList = courseList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return courseList.size();
        }

        @Override
        public Object getItem(int pos) {
            return courseList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
            //just return 0 if your list items do not have an Id variable.
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.item_course_info_list, null);
            }

            //Handle TextView and display string from your list
            TextView courseNameView = (TextView) view.findViewById(R.id.course_info_list_course_name);
            courseNameView.setText(courseList.get(position).courseName);
            TextView descriptionView = (TextView) view.findViewById(R.id.course_info_list_course_description);
            descriptionView.setText(courseList.get(position).getCourseDescription() + " - " + String.valueOf(courseList.get(position).getLength()) + "km");

            //Handle buttons and add onClickListeners
            final Button loadCourseButton = (Button) view.findViewById(R.id.course_info_list_load_button);

            if (loadedCourses.contains(courseList.get(position))) {

                Course foundCourse = loadedCourses.get(loadedCourses.indexOf(courseList.get(position)));

                if (foundCourse.updateCourse(courseList.get(position))) {

                    new DatabaseInserter().execute(new DatabaseParams(
                            Course.class, StaticProjectValues.NAME_OF_COURSE_DATABASE, activityContext, foundCourse));

                }


                loadCourseButton.setEnabled(false);

            } else {

                loadCourseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Course thisCourse = courseList.get(position);
                        loadedCourses.add(thisCourse);
                        thisCourse.dateCreated = DateTime.now();
                        new DatabaseInserter().execute(new DatabaseParams(
                                Course.class, StaticProjectValues.NAME_OF_COURSE_DATABASE, activityContext, thisCourse));

                        loadCourseButton.setEnabled(false);
                    }
                });

            }


            return view;
        }
    }

    @Override
    public void onDBLoadFinished(int startId) {
        switch (startId) {

            case StaticProjectValues.COURSE_DB_LOADER_START_ID:

                if (courseDBLoader.returnList != null) {

                    cloudCourses.addAll(courseDBLoader.returnList);
                    databaseLoaded = true;
                    showProgress((!databaseLoaded || !cloudLoaded));
                    courseListAdapter.notifyDataSetChanged();


                }
                courseDBLoader = null;

                break;

            default:
                break;
        }
    }

    @Override
    public void onAPILoadFinished(int startId) {

        if (startId == 1) {

            if (courseApiAsyncTask.returnList != null) {

                cloudCourses.addAll(courseApiAsyncTask.returnList);
                cloudLoaded = true;
                showProgress((!databaseLoaded || !cloudLoaded));
                courseListAdapter.notifyDataSetChanged();


            }

        }


    }

}
