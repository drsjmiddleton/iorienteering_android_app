package com.racetek.drsjmiddleton.iorienteering;

/**
 * Created by drsjmiddleton on 04/01/2017.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.ArrayList;

public class ConfirmGeneric extends DialogFragment {

    String title;
    String message;
    int objectCode;
    // Use this instance of the interface to deliver action events
    ConfirmGenericListener mListener;

    public ConfirmGeneric() {


    }


    public interface ConfirmGenericListener {
        public void confirmGenericPositiveClick(DialogFragment dialog, int objectCode);
        //public void onSMSStatusDialogNegativeClick(DialogFragment dialog);
    }


    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (ConfirmGenericListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement ConfirmGenericListener");
        }

        Bundle bundle = getArguments();

        if (bundle != null) {

            String[] arguments = bundle.getStringArray("values");

            title = arguments[0];

            message = arguments[1];

            objectCode = Integer.parseInt(arguments[2]);

        }


    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        mListener.confirmGenericPositiveClick(ConfirmGeneric.this, objectCode);

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }


}



