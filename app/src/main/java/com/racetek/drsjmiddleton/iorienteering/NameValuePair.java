package com.racetek.drsjmiddleton.iorienteering;

/**
 * Created by drsjmiddleton on 13/12/2016.
 */
public class NameValuePair {

    private String name;
    private String value;

    public NameValuePair(String name, String value) {

        this.name = name;
        this.value = value;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NameValuePair that = (NameValuePair) o;

        if (!getName().equals(that.getName())) return false;
        return getValue().equals(that.getValue());

    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getValue().hashCode();
        return result;
    }
}
