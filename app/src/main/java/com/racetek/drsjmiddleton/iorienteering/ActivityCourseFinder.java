package com.racetek.drsjmiddleton.iorienteering;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.autosqlite.drsjmiddleton.autosqlite.DbLoaderCallback;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class ActivityCourseFinder extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, ApiLoaderCallback {

    final Context thisActivityContext = this;

    private GoogleMap mMap;
    private ArrayList<Area> areaList;
    SharedPreferences sharedPreferences;
    String token;
    ApiAsyncTask<Area> areaApiAsyncTask;
    LocationManager lm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.course_finder_base);
        Toolbar toolbar = (Toolbar) findViewById(R.id.cf_toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.cf_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.cf_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.cf_map);
        mapFragment.getMapAsync(this);


        sharedPreferences = getSharedPreferences(StaticProjectValues.SHARED_PREFS, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(StaticProjectValues.SHARED_PREFS_API_TOKEN, null);
        areaList = new ArrayList<>();

        lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


        Toast.makeText(getApplicationContext(), R.string.course_finder_wait_for_map,
                Toast.LENGTH_SHORT).show();

        if (token != null) {

            areaApiAsyncTask = (ApiAsyncTask) new ApiAsyncTask<Area>().setCallback(this).execute(

                    new Object[]{
                            1,
                            StaticProjectValues.API_GET_AREA_LIST,
                            new ArrayList<NameValuePair>(),
                            new ArrayList<NameValuePair>(),
                            token,
                            ApiRetriever.GET_JSON_LIST,
                            Area.class
                    }

            );

        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.cf_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_course_finder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

       /* if (id == R.id.nav_camera) {
            // Handle the camera action
        } */

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.cf_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        centreMap(mMap);

    }

    @Override
    public void onAPILoadFinished(int startId) {

        if (startId == 1) {

            if (areaApiAsyncTask.returnList != null) {

                areaList.addAll(areaApiAsyncTask.returnList);
                populateMapDisplay();
                Toast.makeText(getApplicationContext(), R.string.course_finder_map_ready,
                        Toast.LENGTH_SHORT).show();
            }

        }


    }

    private void populateMapDisplay() {

        for (Area area : areaList) {

            mMap.addMarker(new MarkerOptions().position(new LatLng(area.getLatitude(), area.getLongitude())).title(area.getAreaName()));
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    Area areaToView = Area.getAreaFromListByName(marker.getTitle(), areaList);
                    if (areaToView == null) {

                        return false;
                    } else {

                        Intent startViewAreaScreenIntent = new Intent(thisActivityContext, ActivityViewArea.class);

                        startViewAreaScreenIntent.putExtra(StaticProjectValues.INTENT_THIS_AREA, areaToView);

                        startActivity(startViewAreaScreenIntent);

                        return true;
                    }
                }
            });

        }

        centreMap(mMap);

    }

    private void centreMap(GoogleMap mMap) {

        String locationProvider = LocationManager.NETWORK_PROVIDER;

        Location lastKnownLocation = lm.getLastKnownLocation(locationProvider);

        LatLng here = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(here, 6.0f));

    }

}
