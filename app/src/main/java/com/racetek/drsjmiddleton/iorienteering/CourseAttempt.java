package com.racetek.drsjmiddleton.iorienteering;

import android.os.Parcel;
import android.os.Parcelable;

import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteIgnoreFieldAnnotation;
import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLitePrimaryKeyAnnotation;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by drsjmiddleton on 27/09/2016.
 */
public class CourseAttempt implements Parcelable {

    @AutoSQLitePrimaryKeyAnnotation
    String pk;
    Course course;
    String belongsTo;
    boolean finished;
    boolean retired;
    DateTime attemptTime;
    ArrayList<CheckpointTime> checkpointTimes;
    int uploadStatus;


    public CourseAttempt() {

        //empty constructor for AutoSQLlite

    }

    public CourseAttempt(String pk, Course course, String belongsTo, boolean finished, boolean retired, ArrayList<CheckpointTime> checkpointTimes, DateTime attemptTime) {
        this.pk = pk;
        this.course = course;
        this.belongsTo = belongsTo;
        this.finished = finished;
        this.retired = retired;
        this.checkpointTimes = checkpointTimes;
        this.attemptTime = attemptTime;
    }

    public CourseAttempt(String pk, Course course, String belongsTo, DateTime attemptTime, ArrayList<CheckpointTime> checkpointTimes) {
        this.pk = pk;
        this.course = course;
        this.belongsTo = belongsTo;
        this.attemptTime = attemptTime;
        this.checkpointTimes = checkpointTimes;
        this.finished = false;
        this.retired = false;
        this.uploadStatus = CourseAttempt.NOT_UPLOADED;
    }

    public CourseAttempt(Parcel in) {

        this.pk = in.readString();
        this.course = in.readParcelable(getClass().getClassLoader());
        this.belongsTo = in.readString();

        this.finished = in.readByte() != 0;
        this.retired = in.readByte() != 0;

        this.uploadStatus = in.readInt();
        this.attemptTime = new DateTime(in.readLong());

        checkpointTimes = new ArrayList<>();
        in.readTypedList(checkpointTimes, CheckpointTime.CREATOR);


    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeString(pk);
        out.writeParcelable(course, 0);
        out.writeString(belongsTo);

        out.writeByte((byte) (finished ? 1 : 0));
        out.writeByte((byte) (retired ? 1 : 0));

        out.writeInt(uploadStatus);

        out.writeLong(attemptTime.getMillis());
        out.writeTypedList(checkpointTimes);


    }

    @AutoSQLiteIgnoreFieldAnnotation
    public static final Parcelable.Creator<CourseAttempt> CREATOR
            = new Parcelable.Creator<CourseAttempt>() {
        public CourseAttempt createFromParcel(Parcel in) {
            return new CourseAttempt(in);
        }

        public CourseAttempt[] newArray(int size) {
            return new CourseAttempt[size];
        }
    };

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public boolean getFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public String getBelongsTo() {
        return belongsTo;
    }

    public void setBelongsTo(String belongsTo) {
        this.belongsTo = belongsTo;
    }

    public boolean getRetired() {
        return retired;
    }

    public void setRetired(boolean retired) {
        this.retired = retired;
    }

    public DateTime getAttemptTime() {
        return attemptTime;
    }

    public void setAttemptTime(DateTime attemptTime) {
        this.attemptTime = attemptTime;
    }

    public ArrayList<CheckpointTime> getCheckpointTimes() {
        return checkpointTimes;
    }

    public void setCheckpointTimes(ArrayList<CheckpointTime> checkpointTimes) {
        this.checkpointTimes = checkpointTimes;
    }

    public int getUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public DateTime getFinishTime() {

        Collections.sort(checkpointTimes, new CheckpointTime.cptOrderComparator());

        if (!finished) {

            return null;
        } else {

            try {
                return checkpointTimes.get(checkpointTimes.size() - 1).getCheckpointTime();
            } catch (IndexOutOfBoundsException e) {

                return null;
            }

        }

    }

    public DateTime getLastControlTime() {

        Collections.sort(checkpointTimes, new CheckpointTime.cptOrderComparator());

        try {
            return checkpointTimes.get(checkpointTimes.size() - 1).getCheckpointTime();
        } catch (IndexOutOfBoundsException e) {

            return null;
        }

    }

    public DateTime getStartTime() {

        Collections.sort(checkpointTimes, new CheckpointTime.cptOrderComparator());

        try {
            return checkpointTimes.get(0).getCheckpointTime();
        } catch (IndexOutOfBoundsException e) {

            return null;
        }

    }

    public long getCourseTime() {

        if (!finished) {

            return 0;
        } else {

            try {
                return getFinishTime().getMillis() - getStartTime().getMillis();
            } catch (Exception e) {
                return 0;
            }
        }

    }

    public long getOngoingCourseTime() {

        try {
            return getLastControlTime().getMillis() - getStartTime().getMillis();
        } catch (Exception e) {
            return 0;
        }
    }

    private ValidatedCourseAttempt validateEndToEnd() {

        Collections.sort(checkpointTimes, new CheckpointTime.cptOrderComparator());

        ArrayList<String> attemptSortedIdList = new ArrayList<>();

        ArrayList<CheckpointTime> sortedValidatedCPTs = new ArrayList<>();

        ArrayList<String> missingCpIds = new ArrayList<>();

        boolean dsq = false;

        boolean outerWrongOrder = false;

        boolean dnf = false;

        int status = ValidatedCourseAttempt.STATUS_COMPLETED;

        String dsqReason = "";

        for (CheckpointTime cpt : checkpointTimes) {


            attemptSortedIdList.add(cpt.checkpointID);

        }

        ArrayList<String> checkpointIdsList = Lists.newArrayList(
                Splitter.fixedLength(3)
                        .split(course.courseCodes));

        int lastCorrectIndex = -1;

        for (String cpId : checkpointIdsList) {


            boolean foundInWrongOrder = false;
            boolean foundInCorrectOrder = false;

            int lastFoundIndex = -1;

            int lastWrongOrderIndex = -1;

            while (attemptSortedIdList.indexOf(cpId) > -1) {

                lastFoundIndex = attemptSortedIdList.indexOf(cpId);

                if (!foundInCorrectOrder) {

                    if (lastFoundIndex > lastCorrectIndex) {

                        lastCorrectIndex = lastFoundIndex;

                        sortedValidatedCPTs.add(checkpointTimes.get(lastCorrectIndex));
                        foundInCorrectOrder = true;
                        foundInWrongOrder = false;
                        break;

                    } else {

                        lastWrongOrderIndex = attemptSortedIdList.indexOf(cpId);
                        foundInWrongOrder = true;

                        attemptSortedIdList.set(lastFoundIndex, null);

                    }

                }

            }

            if (foundInCorrectOrder) {

                continue;

            } else if (foundInWrongOrder) {

                checkpointTimes.get(lastWrongOrderIndex).cptStatus = CheckpointTime.WRONG_ORDER_CPT;

                sortedValidatedCPTs.add(checkpointTimes.get(lastWrongOrderIndex));

                dsq = true;

                outerWrongOrder = true;
            } else {

                sortedValidatedCPTs.add(new CheckpointTime(pk, cpId, -1, null, CheckpointTime.MISSING_CPT)
                );

                missingCpIds.add(cpId);

                if (checkpointIdsList.indexOf(cpId) + 1 == checkpointIdsList.size()) {

                    dnf = true;

                }

                dsq = true;

            }

        }

        //final check the times are ordered correctly

        if (!dsq) {
            for (int i = 0; i < sortedValidatedCPTs.size() - 1; i++) {


                if (sortedValidatedCPTs.get(i + 1).checkpointTime.getMillis() - sortedValidatedCPTs.get(i).checkpointTime.getMillis() < 1) {

                    dsq = true;
                    outerWrongOrder = true;
                    break;

                }

            }
        }


        //dsq reason
        if (missingCpIds.size() > 0) {

            if (dnf) {

                dsqReason = "Retired.";

                status = ValidatedCourseAttempt.STATUS_RTD;
            } else {
                dsqReason = "Missing controls with IDs ";

                status = ValidatedCourseAttempt.STATUS_MISSING;

                for (String cpId : missingCpIds) {

                    dsqReason += cpId + " ,";
                }

                dsqReason = dsqReason.substring(0, dsqReason.length() - 2);

                dsqReason += ".";

                if (dsqReason.length() > 100) {

                    dsqReason = "Retired.";
                }
            }
        } else if (outerWrongOrder == true) {


            dsqReason = "Controls visited in incorrect order.";

            status = ValidatedCourseAttempt.STATUS_DSQ;

        }


        return new ValidatedCourseAttempt(this, status, sortedValidatedCPTs, dsqReason);

    }

    private ValidatedCourseAttempt validateScore() {

        boolean dsq = false;

        int status = ValidatedCourseAttempt.STATUS_COMPLETED;

        Collections.sort(checkpointTimes, new CheckpointTime.cptOrderComparator());

        ArrayList<CheckpointTime> uniqueCPTList = new ArrayList<>();

        for (CheckpointTime cpt : checkpointTimes) {

            if (!CheckpointTime.doesListContainSameCheckpoint(uniqueCPTList, cpt)) {

                uniqueCPTList.add(cpt);
            }

        }

        Collections.sort(uniqueCPTList, new CheckpointTime.cptOrderComparator());

        if (retired) {

            status = ValidatedCourseAttempt.STATUS_RTD;
        } else if (!finished) {

            status = ValidatedCourseAttempt.STATUS_DNF;
        }



        int totalPoints = getCurrentPointsTotal();

        long elapsedCourseTime = uniqueCPTList.get(uniqueCPTList.size() - 1).checkpointTime.getMillis() - uniqueCPTList.get(0).checkpointTime.getMillis();

        long overdue = (elapsedCourseTime - course.getTimeLimit() * 60 * 1000) / 1000;

        int penalty = 0;

        if (overdue > 0) {

            penalty = (int) Math.ceil(overdue * course.penalty);
        }

        return new ValidatedCourseAttempt(this, uniqueCPTList, status, "Retired or hasn't finished", totalPoints, penalty);

    }

    public ValidatedCourseAttempt validateAttempt() {

        if (course.isScore) {

            return validateScore();

        } else {
            return validateEndToEnd();
        }
    }

    public int getCurrentPointsTotal() {

        int runningTotal = 0;


        if (!course.isScore || course.checkpointPointsMap == null) {

            return 0;
        } else {

            HashSet<String> uniqueCPList = new HashSet<>();

            for (CheckpointTime cpt : checkpointTimes) {


                if (uniqueCPList.add(cpt.checkpointID)) {

                    runningTotal += course.checkpointPointsMap.get(cpt.checkpointID);

                }

            }

            return runningTotal;
        }

    }



    public String getNextCode() {

        Collections.sort(checkpointTimes, new CheckpointTime.cptOrderComparator());

        ArrayList<String> attemptSortedIdList = new ArrayList<>();

        for (CheckpointTime cpt : checkpointTimes) {


            attemptSortedIdList.add(cpt.checkpointID);

        }

        ArrayList<String> checkpointIdsList = Lists.newArrayList(
                Splitter.fixedLength(3)
                        .split(course.courseCodes));


        int lastCorrectIndex = -1;
        int lastCorrectCpIDindex = -1;

        for (int i = 0; i < checkpointIdsList.size(); i++) {

            String cpId = checkpointIdsList.get(i);

            int lastFoundIndex = -1;

            boolean foundID = false;

            while (attemptSortedIdList.indexOf(cpId) > -1) {

                lastFoundIndex = attemptSortedIdList.indexOf(cpId);

                if (lastFoundIndex > lastCorrectIndex) {

                    lastCorrectIndex = lastFoundIndex;

                    lastCorrectCpIDindex = i;

                    foundID = true;

                    break;

                } else {

                    attemptSortedIdList.set(lastFoundIndex, null);

                }


            }

            if (!foundID) {

                lastCorrectCpIDindex = i - 1;
                break;

            }


        }

        try {
            return checkpointIdsList.get(lastCorrectCpIDindex + 1);
        } catch (IndexOutOfBoundsException e) {
            return "";
        }

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CourseAttempt that = (CourseAttempt) o;

        return getPk().equals(that.getPk());

    }

    @Override
    public int hashCode() {
        return getPk().hashCode();
    }

    public final static int NOT_UPLOADED = 0;
    public final static int UPLOADED = 1;


}
