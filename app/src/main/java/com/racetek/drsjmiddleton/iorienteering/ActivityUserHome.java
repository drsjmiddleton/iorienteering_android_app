package com.racetek.drsjmiddleton.iorienteering;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteDbFromObjectHelper;
import com.autosqlite.drsjmiddleton.autosqlite.AutoSQLiteIncorrectObjectForDbException;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseLoader;
import com.autosqlite.drsjmiddleton.autosqlite.DatabaseParams;
import com.autosqlite.drsjmiddleton.autosqlite.DbLoaderCallback;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ActivityUserHome extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DbLoaderCallback, ConfirmGeneric.ConfirmGenericListener {


    ListView courseListView;

    TextView noCourseTextView;

    String username;

    ArrayList<Course> courseList;

    CourseListAdapter courseListAdapter;

    DatabaseLoader courseDBLoader;
    DatabaseLoader courseAttemptDBLoader;

    Button quickStartButton;
    Button continueLastAttempt;

    CourseAttempt currentCourseAttempt;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    final Context thisActivityContext = this;

    boolean confirmLogout = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_home_base);
        Toolbar toolbar = (Toolbar) findViewById(R.id.user_home_toolbar);
        toolbar.setTitle("Home");
        setSupportActionBar(toolbar);

        sharedPreferences = getSharedPreferences(StaticProjectValues.SHARED_PREFS, Context.MODE_PRIVATE);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        courseListView = (ListView) findViewById(R.id.home_course_list);
        noCourseTextView = (TextView) findViewById(R.id.home_no_courses_message);
        quickStartButton = (Button) findViewById(R.id.home_quick_start_button);
        quickStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startQRActivity = new Intent(getApplicationContext(), ActivityQRCourseReader.class);
                startActivity(startQRActivity);
            }
        });
        continueLastAttempt = (Button) findViewById(R.id.home_continue_last_attempt_button);
        continueLastAttempt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startQRScreenIntent = new Intent(thisActivityContext, ActivityQRCourseReader.class);

                startQRScreenIntent.putExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT, currentCourseAttempt);

                startActivity(startQRScreenIntent);
            }
        });
        continueLastAttempt.setEnabled(false);

        courseList = new ArrayList<>();
        courseListAdapter = new CourseListAdapter(courseList, this);

        courseListView.setAdapter(courseListAdapter);

    }


    public void confirmGenericPositiveClick(DialogFragment dialog, int objectCode) {

        confirmLogout = true;

        onBackPressed();

    }


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (confirmLogout) {

                editor = sharedPreferences.edit();

                editor.putString(StaticProjectValues.SHARED_PREFS_USERNAME, "");
                editor.putString(StaticProjectValues.SHARED_PREFS_API_TOKEN, "");
                editor.putBoolean(StaticProjectValues.SHARED_PREFS_IS_LOGGED_IN, false);
                editor.putString(StaticProjectValues.SHARED_PREFS_PK_OF_CURRENT_ATTEMPT, "");

                editor.apply();

                //clear database of all data items

                clearDB();

                Intent startLogin = new Intent(this, ActivityLoginScreen.class);

                finish();

                startActivity(startLogin);


            } else {

                DialogFragment confirmLogoutDialog = new ConfirmGeneric();

                String[] values = {
                        getString(R.string.user_home_screen_confirm_logout_title),
                        getString(R.string.user_home_screen_confirm_logout_message),
                        "1"
                };

                Bundle bundle = new Bundle();

                bundle.putStringArray("values", values);

                confirmLogoutDialog.setArguments(bundle);

                confirmLogoutDialog.show(getSupportFragmentManager(), "ConfirmLogout");

            }

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_user_home, menu);
        return true;
    }

    @Override
    public void onResume() {

        populateFromDB();
        super.onResume();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_view_results) {

            Intent startResultIntent = new Intent(this, ActivityResults.class);

            startActivity(startResultIntent);
            // Handle the camera action
        } else if (id == R.id.nav_find_area) {

            Intent startResultIntent = new Intent(this, ActivityCourseFinder.class);

            startActivity(startResultIntent);

        }


        /*if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class CourseListAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<Course> courseList = new ArrayList<Course>();
        private Context context;


        public CourseListAdapter(ArrayList<Course> courseList, Context context) {
            this.courseList = courseList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return courseList.size();
        }

        @Override
        public Object getItem(int pos) {
            return courseList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
            //just return 0 if your list items do not have an Id variable.
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.course_list_item, null);
            }

            //Handle TextView and display string from your list
            TextView courseNameView = (TextView) view.findViewById(R.id.course_list_course_name);
            courseNameView.setText(courseList.get(position).courseName);
            TextView areaNameView = (TextView) view.findViewById(R.id.course_list_area_name);
            areaNameView.setText(courseList.get(position).areaName);

            //Handle buttons and add onClickListeners
            Button startCourse = (Button) view.findViewById(R.id.course_list_enter_button);


            startCourse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CourseAttempt courseAttemptToStart = new CourseAttempt(String.valueOf(DateTime.now().getMillis()),
                            courseList.get(position),
                            username,
                            DateTime.now(),
                            new ArrayList<CheckpointTime>()
                    );

                    Intent startQRScreenIntent = new Intent(context, ActivityQRCourseReader.class);

                    startQRScreenIntent.putExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT, courseAttemptToStart);

                    startActivity(startQRScreenIntent);

                }
            });


            return view;
        }
    }

    public class CourseAttemptListAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<CourseAttempt> courseAttemptList = new ArrayList<CourseAttempt>();
        private Context context;


        public CourseAttemptListAdapter(ArrayList<CourseAttempt> courseAttemptList, Context context) {
            this.courseAttemptList = courseAttemptList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return courseAttemptList.size();
        }

        @Override
        public Object getItem(int pos) {
            return courseAttemptList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
            //just return 0 if your list items do not have an Id variable.
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.course_list_item, null);
            }

            //Handle TextView and display string from your list
            TextView courseNameView = (TextView) view.findViewById(R.id.course_list_course_name);
            courseNameView.setText(courseAttemptList.get(position).course.courseName);
            TextView areaNameView = (TextView) view.findViewById(R.id.course_list_area_name);
            areaNameView.setText(courseAttemptList.get(position).course.areaName);

            //Handle buttons and add onClickListeners
            Button startCourse = (Button) view.findViewById(R.id.course_list_enter_button);


            startCourse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent startQRScreenIntent = new Intent(context, ActivityQRCourseReader.class);

                    startQRScreenIntent.putExtra(StaticProjectValues.INTENT_THIS_COURSE_ATTEMPT, courseAttemptList.get(position));

                    startActivity(startQRScreenIntent);

                }
            });


            return view;
        }
    }

    @Override
    public void onDBLoadFinished(int startId) {
        switch (startId) {

            case StaticProjectValues.COURSE_DB_LOADER_START_ID:

                if (courseDBLoader.returnList == null || courseDBLoader.returnList.size() == 0) {

                    courseListView.setVisibility(View.GONE);
                    noCourseTextView.setVisibility(View.VISIBLE);


                } else {

                    courseListView.setVisibility(View.VISIBLE);
                    noCourseTextView.setVisibility(View.GONE);

                    courseList.clear();
                    for (Object course : courseDBLoader.returnList) {

                        courseList.add((Course) course);
                        Collections.sort(courseList);
                        courseListAdapter.notifyDataSetChanged();

                    }
                }
                courseDBLoader = null;

                break;
            case StaticProjectValues.COURSE_ATTEMPT_DB_LOADER_START_ID:


                if (courseAttemptDBLoader.returnObject != null) {

                    currentCourseAttempt = (CourseAttempt) courseAttemptDBLoader.returnObject;
                    continueLastAttempt.setEnabled(true);

                }

                courseAttemptDBLoader = null;

                break;
            default:
                break;
        }
    }

    private void populateFromDB() {

        if (courseAttemptDBLoader == null) {

            String pkOfCurrentAttempt = sharedPreferences.getString(StaticProjectValues.SHARED_PREFS_PK_OF_CURRENT_ATTEMPT, null);

            if (pkOfCurrentAttempt != null) {

                courseAttemptDBLoader = new DatabaseLoader();


                DatabaseParams courseAttemptDblp = new DatabaseParams(
                        StaticProjectValues.COURSE_ATTEMPT_DB_LOADER_START_ID,
                        CourseAttempt.class,
                        StaticProjectValues.NAME_OF_COURSE_ATTEMPT_DATABASE,
                        this,
                        pkOfCurrentAttempt
                );

                Object[] argsArray = {courseAttemptDblp};

                courseAttemptDBLoader.setCallback(this).execute(argsArray);

            }

        }

        if (courseDBLoader == null) {

            courseDBLoader = new DatabaseLoader();

            DatabaseParams courseDblp = new DatabaseParams(
                    StaticProjectValues.COURSE_DB_LOADER_START_ID,
                    Course.class,
                    StaticProjectValues.NAME_OF_COURSE_DATABASE,
                    this,
                    DatabaseParams.LOAD_ALL);

            //hacky way of avoiding the classcastexception
            Object[] argsArray2 = {courseDblp};


            courseDBLoader.setCallback(this).execute(argsArray2);

        }


    }

    private ArrayList<Object> getAllCourseAttempts() {

        AutoSQLiteDbFromObjectHelper dbHelper = new AutoSQLiteDbFromObjectHelper(this, new ArrayList<Class>(Collections.singletonList(CourseAttempt.class)), StaticProjectValues.NAME_OF_COURSE_ATTEMPT_DATABASE);

        ArrayList<Object> courseAttemptArrayList = new ArrayList<>();


        try {

            ArrayList<String> dbTables = dbHelper.getListOfDBTables();
            courseAttemptArrayList = (ArrayList<Object>) dbHelper.getAllObjectsFromDb(CourseAttempt.class);


        } catch (AutoSQLiteIncorrectObjectForDbException e) {
            e.printStackTrace();
        } catch (SQLException e) {

            e.printStackTrace();


        }

        String pkOfCurrentAttempt = sharedPreferences.getString(StaticProjectValues.SHARED_PREFS_PK_OF_CURRENT_ATTEMPT, null);

        ArrayList<Object> returnAttemptArrayList = new ArrayList<>(courseAttemptArrayList);


        return returnAttemptArrayList;

    }


    private void clearDB() {


        for (int i = 0; i < StaticProjectValues.DATABASE_NAMES_ARRAY.length; i++) {

            AutoSQLiteDbFromObjectHelper dbHelper = new AutoSQLiteDbFromObjectHelper(this, new ArrayList<Class>(Collections.singletonList(StaticProjectValues.DATABASE_CLASSES_ARRAY[i])), StaticProjectValues.DATABASE_NAMES_ARRAY[i]);

            dbHelper.deleteDB(this, StaticProjectValues.DATABASE_NAMES_ARRAY[i]);


        }

    }

}
