package com.racetek.drsjmiddleton.iorienteering;

import android.os.AsyncTask;

import java.util.ArrayList;


/**
 * Created by drsjmiddleton on 13/12/2016.
 */

/*
   The parameters must ob an object[] with the following order:


        callbackID = (Integer) params[0];

        this.uri = (String) params[1];
        this.headerSet = (ArrayList<NameValuePair>) params[2];
        this.parameterSet =(ArrayList<NameValuePair>) params[3];
        this.token = (String) params[4];
        this.function = (int) params[5];
        this.typeParameterClass = (Class<T>) params[6];
    */
public class ApiAsyncTask<T> extends AsyncTask<Object, Void, Integer> {

    public ArrayList<T> returnList;

    protected ApiLoaderCallback callback = null; //interface implemented for processing response

    public ApiAsyncTask setCallback(ApiLoaderCallback callback) {
        this.callback = callback;
        return this;
    }


    @Override
    protected Integer doInBackground(Object... oParams) {

        ApiRetriever<T> retriever = new ApiRetriever(oParams);

        if (retriever.function == ApiRetriever.GET_JSON_LIST) {

            returnList = retriever.returnList;
        }


        return (Integer) oParams[0];
    }

    @Override
    final protected void onPostExecute(Integer startId) {
        //no overrides, same every time
        if (callback != null && startId > 0) {
            callback.onAPILoadFinished(startId);  //forward generic result, but there it will be typed
        }
    }


    private <T> ArrayList<T> getList(Class<T> requiredType) {
        return new ArrayList<T>();
    }


}