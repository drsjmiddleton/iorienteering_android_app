package com.racetek.drsjmiddleton.iorienteering;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by drsjmiddleton on 04/01/2017.
 */

@Root(name = "Result")
@Order(elements = {"StartTime", "FinishTime", "Time", "Status"})
public class XMLCourseAttempt {

    @Element(name = "Time", required = false)
    long time;
    @Element(name = "Status")
    String status;
    @Element(name = "StartTime")
    String startTime;
    @Element(name = "FinishTime", required = false)
    String finishTime;

    @ElementList(inline = true)
    ArrayList<SplitTime> splitTimes;

    public XMLCourseAttempt(ValidatedCourseAttempt vca) {

        this.time = vca.getCourseTime() / 1000;

        switch ((vca.status)) {

            case ValidatedCourseAttempt.STATUS_COMPLETED:
                this.status = "OK";
                break;
            case ValidatedCourseAttempt.STATUS_DNF:
                this.status = "DidNotFinish";
                break;
            case ValidatedCourseAttempt.STATUS_DSQ:
                this.status = "Disqualified";
                break;
            case ValidatedCourseAttempt.STATUS_RTD:
                this.status = "DidNotFinish";
                break;
            case ValidatedCourseAttempt.STATUS_MISSING:
                this.status = "MissingPunch";
                break;
            default:
                this.status = "DidNotEnter";
                break;

        }

        DateTimeFormatter fmt = ISODateTimeFormat.dateTime();

        this.splitTimes = new ArrayList<>();

        for (int i = 0; i < vca.sortedValidatedCPTs.size(); i++) {

            if (i == 0) {

                this.startTime = vca.sortedValidatedCPTs.get(i).checkpointTime.toString(fmt);
            } else if (i == vca.sortedValidatedCPTs.size() - 1) {

                if (vca.sortedValidatedCPTs.get(i).checkpointTime.getMillis() < 3) {

                    continue;
                }

                this.finishTime = vca.sortedValidatedCPTs.get(i).checkpointTime.toString(fmt);

                //this.time= (long) (vca.sortedValidatedCPTs.get(i).checkpointTime.getMillis()-vca.sortedValidatedCPTs.get(0).checkpointTime.getMillis())/1000;


            } else {

                if (vca.sortedValidatedCPTs.get(i).checkpointTime.getMillis() < 3) {

                    continue;
                }

                double cumulative = (double) (vca.sortedValidatedCPTs.get(i).checkpointTime.getMillis() - vca.sortedValidatedCPTs.get(0).checkpointTime.getMillis()) / 1000;

                splitTimes.add(new SplitTime(vca.sortedValidatedCPTs.get(i).checkpointID, cumulative));

            }
        }


    }

    public long getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }

    public ArrayList<SplitTime> getSplitTimes() {
        return splitTimes;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }
}
