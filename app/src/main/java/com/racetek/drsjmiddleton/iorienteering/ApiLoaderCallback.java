package com.racetek.drsjmiddleton.iorienteering;

/**
 * Created by drsjmiddleton on 15/12/2016.
 */
public interface ApiLoaderCallback {

    void onAPILoadFinished(int response);


}
